#!/bin/bash

if [ ! -d /tmp/r24zeng/cache ]
then
  mkdir /tmp/r24zeng/cache
fi

if [ ! -d /tmp/r24zeng/mount ]
then
  mkdir /tmp/r24zeng/mount
fi

fusermount -u /tmp/r24zeng/mount
./watdfs_client -s -f -o nonempty /tmp/r24zeng/cache /tmp/r24zeng/mount
./watdfs_client -s -f -o direct_io /tmp/r24zeng/cache /tmp/r24zeng/mount
