//
// Starter code for CS 454/654
// You SHOULD change this file
//

#include "rpc.h"
#include "debug.h"

INIT_LOG

#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <cstring>
#include <cstdlib>
#include <fuse.h>
#include <fcntl.h>
#include <iostream>


// Global state server_persist_dir.
char *server_persist_dir = nullptr;

// Important: the server needs to handle multiple concurrent client requests.
// You have to be carefully in handling global variables, esp. for updating them.
// Hint: use locks before you update any global variable.

// We need to operate on the path relative to the the server_persist_dir.
// This function returns a path that appends the given short path to the
// server_persist_dir. The character array is allocated on the heap, therefore
// it should be freed after use.
// Tip: update this function to return a unique_ptr for automatic memory management.
char *get_full_path(char *short_path) {
    int short_path_len = strlen(short_path);
    int dir_len = strlen(server_persist_dir);
    int full_len = dir_len + short_path_len + 1;

    char *full_path = (char *)malloc(full_len);

    // First fill in the directory.
    strcpy(full_path, server_persist_dir);
    // Then append the path.
    strcat(full_path, short_path);
    DLOG("Full path: %s\n", full_path);

    return full_path;
}

// The server implementation of getattr.
int watdfs_getattr(int *argTypes, void **args) {
    // Get the arguments.
    // The first argument is the path relative to the mountpoint.
    char *short_path = (char *)args[0];
    // The second argument is the stat structure, which should be filled in
    // by this function.
    struct stat *statbuf = (struct stat *)args[1];
    // The third argument is the return code, which should be set be 0 or -errno.
    int *ret = (int *)args[2];

    // Get the local file name, so we call our helper function which appends
    // the server_persist_dir to the given path.
    char *full_path = get_full_path(short_path);

    // Initially we set set the return code to be 0.
    *ret = 0;

    // TODO: Make the stat system call, which is the corresponding system call needed
    // to support getattr. You should use the statbuf as an argument to the stat system call.
    (void)statbuf;


    // Let sys_ret be the return code from the stat system call.
    int sys_ret = 0;
    DLOG("start sys_stat...");
    sys_ret = stat(full_path, statbuf);

    if (sys_ret < 0) {
        // If there is an error on the system call, then the return code should
        // be -errno.
        DLOG("sys_stat fail");
        *ret = -errno;
    }
    else{
        *ret = 0;
        DLOG("sys_stat success");
    }

    // Clean up the full path, it was allocated on the heap.
    free(full_path);

    DLOG("Returning code: %d", *ret);
    // The RPC call succeeded, so return 0.
    return 0;
}

int watdfs_fgetattr(int *argTypes, void **args){
    DLOG("start sys_rfgetattr...");
    char *short_path = (char *)args[0];
    struct stat *statbuf = (struct stat *)args[1];
    struct fuse_file_info *fi = (struct fuse_file_info *)args[2];
    int *ret = (int *)args[3];
    char *full_path = get_full_path(short_path);
    // Initially we set set the return code to be 0.
    *ret = 0;
    int sys_ret;
    (void)full_path;
    (void)statbuf;
    sys_ret = fstat(fi->fh, statbuf);
    // handle error
    if(sys_ret == -1){
        DLOG("sys_fgetattr fail");
        *ret = -errno;
    }
    else{
        *ret = 0;
        DLOG("sys_fgetattr success");
    }

    // Clean up the full path, it was allocated on the heap.
    free(full_path);

    DLOG("Returning code: %d", *ret);
    // The RPC call succeeded, so return 0.
    return 0;
}

int watdfs_mknod(int *argTypes, void **args){
    DLOG("start sys_mknod...");
    // Get the arguments.
    // The first argument is the path relative to the mountpoint.
    char *short_path = (char *)args[0];
    // The second argument is the stat structure, which should be filled in
    // by this function.
    mode_t *mode = (mode_t *)args[1];
    // The second argument is the stat structure, which should be filled in
    // by this function.
    dev_t *dev = (dev_t *)args[2];
    // The third argument is the return code, which should be set be 0 or -errno.
    int *ret = (int *)args[3];

    // Get the local file name, so we call our helper function which appends
    // the server_persist_dir to the given path.
    char *full_path = get_full_path(short_path);

    // Initially we set set the return code to be 0.
    *ret = 0;

    // Let sys_ret be the return code from the stat system call.
    int sys_ret = 0;

    sys_ret = mknod(full_path, *mode, *dev);

    if (sys_ret < 0) {
        // If there is an error on the system call, then the return code should
        // be -errno.
        DLOG("sys_mknod fail");
        *ret = -errno;
    }
    else{
        *ret = 0;
        DLOG("sys_mknod success");;
    }

    // Clean up the full path, it was allocated on the heap.
    free(full_path);

    DLOG("Returning code: %d", *ret);
    // The RPC call succeeded, so return 0.
    return 0;
}

int watdfs_open(int *argTypes, void **args){
    DLOG("start sys_open...");
    // path relative to the mountpoint.
    char *short_path = (char *)args[0];
    // struct fuse_file_info *fi
    struct fuse_file_info *fi = (struct fuse_file_info *)args[1];
    // retcode, should be 0 or errno
    int *ret = (int *)args[2];

    char *full_path = get_full_path(short_path);
    // Initially we set set the return code to be 0.
    *ret = 0;

    // Let sys_ret be the return code from the stat system call.
    int sys_ret = 0;

    sys_ret = open(full_path, fi->flags);
    //fi->fh = sys_ret;

    if(sys_ret < 0){
        DLOG("sys_open fail");

        *ret = -errno;
    }
    else{
        *ret = 0;
        fi->fh = sys_ret;
        DLOG("sys_open success");

    }

    // Clean up the full path, it was allocated on the heap.
    free(full_path);
    DLOG("filehandle is : %d", sys_ret);
    DLOG("Returning code: %d", *ret);
    // The RPC call succeeded, so return 0.
    return 0;
}

int watdfs_release(int *argTypes, void **args){
    DLOG("start sys_close ...");
    char *short_path = (char *)args[0];
    struct fuse_file_info *fi = (struct fuse_file_info *)args[1];
    int *ret = (int *)args[2];

    char *full_path = get_full_path(short_path);
    *ret = 0;
    int sys_ret = 0;

    sys_ret = close(fi->fh);
    if(sys_ret == -1){
        DLOG("sys_close fail");
        *ret = -errno;
    }
    else{
        *ret = sys_ret;
        DLOG("sys_close success");
    }

    free(full_path);
    return 0;
}

int watdfs_read(int *argTypes, void **args){
    DLOG("start sys_read...");
    char *short_path = (char *)args[0];
    char *buf = (char *)args[1];
    size_t *size = (size_t *)args[2];
    off_t *offset = (off_t *)args[3];
    struct fuse_file_info *fi = (struct fuse_file_info *)args[4];
    int *ret = (int *)args[5];
    char *full_path = get_full_path(short_path);
    // Initially we set set the return code to be 0.
    //*ret = 0;
    int sys_ret;
    (void)full_path;
    DLOG("before call sys_read");
    sys_ret = pread(fi->fh, buf, *size, *offset);
//    DLOG("[read] buf on server is : %s", buf);
    DLOG("return number of byte from sys_pread: %d", sys_ret);

    // handle error
    if(sys_ret < 0){
        DLOG("sys_read fail");
        *ret = -errno;
    }
    else{
        DLOG("sys_read success");
        *ret = sys_ret;
    }

    // Clean up the full path, it was allocated on the heap.
    free(full_path);

    DLOG("Returning code: %d", *ret);
    // The RPC call succeeded, so return 0.
    return *ret;
}

int watdfs_write(int *argTypes, void **args){
    DLOG("start sys_write...");
    char *short_path = (char *)args[0];
    const char *buf = (const char *)args[1];
    size_t *size = (size_t *)args[2];
    off_t *offset = (off_t *)args[3];
    struct fuse_file_info *fi = (struct fuse_file_info *)args[4];
    int *ret = (int *)args[5];
    char *full_path = get_full_path(short_path);
    // Initially we set set the return code to be 0.
    //*ret = 0;
    int sys_ret;
    (void)full_path;
    sys_ret = pwrite(fi->fh, buf, *size, *offset);
//    DLOG("[write] buf on server is : %s", buf);
    // handle error
    if(sys_ret < 0){
        DLOG("sys_write fail");
        *ret = -errno;
    }
    else{
        DLOG("sys_write success");
        *ret = sys_ret;
    }

    // Clean up the full path, it was allocated on the heap.
    free(full_path);

    DLOG("Returning code: %d", *ret);
    // The RPC call succeeded, so return 0.
    return *ret;
}

int watdfs_truncate(int *argTypes, void **args){
    DLOG("start sys_truncate...");
    char *short_path = (char *)args[0];
    off_t *newsize = (off_t *)args[1];
    int *ret = (int *)args[2];
    char *full_path = get_full_path(short_path);
    // Initially we set set the return code to be 0.
    *ret = 0;
    int sys_ret;
    (void)full_path;
    sys_ret = truncate(full_path, *newsize);
    // handle error
    if(sys_ret < 0){
        DLOG("sys_truncate fail");
        *ret = -errno;
    }
    else{
        *ret = sys_ret;
        DLOG("sys_truncate success");
    }

    // Clean up the full path, it was allocated on the heap.
    free(full_path);

    DLOG("Returning code: %d", *ret);
    // The RPC call succeeded, so return 0.
    return 0;
}

int watdfs_utimens(int *argTypes, void **args){
    DLOG("start sys_utimens...");
    char *short_path = (char *)args[0];
    struct timespec *ts = (struct timespec *)args[1];
    int *ret = (int *)args[2];
    char *full_path = get_full_path(short_path);
    // Initially we set set the return code to be 0.
    *ret = 0;
    int sys_ret;
    (void)full_path;
    sys_ret = utimensat(0, full_path, ts, 0);
    // handle error
    if(sys_ret < 0){
        DLOG("sys_utimens fail");
        *ret = -errno;
    }
    else{
        *ret = sys_ret;
        DLOG("sys_utimens success");
    }

    // Clean up the full path, it was allocated on the heap.
    free(full_path);

    DLOG("Returning code: %d", *ret);
    // The RPC call succeeded, so return 0.
    return 0;
}

int watdfs_fsync(int *argTypes, void **args){
    DLOG("start sys_fsync...");
    char *short_path = (char *)args[0];
    struct fuse_file_info *fi = (struct fuse_file_info *)args[1];
    int *ret = (int *)args[2];
    char *full_path = get_full_path(short_path);
    // Initially we set set the return code to be 0.
    *ret = 0;
    int sys_ret;
    (void)full_path;
    sys_ret = fsync(fi->fh);
    // handle error
    if(sys_ret < 0){
        DLOG("sys_fsync fail");
        *ret = -errno;
    }
    else{
        *ret = sys_ret;
        DLOG("sys_fsync success");
    }

    // Clean up the full path, it was allocated on the heap.
    free(full_path);

    DLOG("Returning code: %d", *ret);
    // The RPC call succeeded, so return 0.
    return 0;
}


// The main function of the server.
int main(int argc, char *argv[]) {
    // argv[1] should contain the directory where you should store data on the
    // server. If it is not present it is an error, that we cannot recover from.
    if (argc != 2) {
        // In general you shouldn't print to stderr or stdout, but it may be
        // helpful here for debugging. Important: Make sure you turn off logging
        // prior to submission!
        // See watdfs_client.c for more details
        // # ifdef PRINT_ERR
        // std::cerr << "Usaage:" << argv[0] << " server_persist_dir";
        // #endif
        return -1;
    }
    // Store the directory in a global variable.
    server_persist_dir = argv[1];

    // TODO: Initialize the rpc library by calling `rpcServerInit`.
    int rpcInit = rpcServerInit();
    // Important: `rpcServerInit` prints the 'export SERVER_ADDRESS' and
    // 'export SERVER_PORT' lines. Make sure you *do not* print anything
    // to *stdout* before calling `rpcServerInit`.
    //DLOG("Initializing server...");

    int ret = 0;
    // TODO: If there is an error with `rpcServerInit`, it maybe useful to have
    // debug-printing here, and then you should return.

    if(rpcInit == 0){
        DLOG("Initializing server...");
    }
    else{
        DLOG("Initializing server error ... ");
    }

    // TODO: Register your functions with the RPC library.
    // Note: The braces are used to limit the scope of `argTypes`, so that you can
    // reuse the variable for multiple registrations. Another way could be to
    // remove the braces and use `argTypes0`, `argTypes1`, etc.
    // register "getattr"
    {
        // There are 3 args for the function (see watdfs_client.c for more
        // detail).
        int argTypes[4];
        // First is the path.
        argTypes[0] =
            (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        // The second argument is the statbuf.
        argTypes[1] =
            (1u << ARG_OUTPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        // The third argument is the retcode.
        argTypes[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
        // Finally we fill in the null terminator.
        argTypes[3] = 0;

        // We need to register the function with the types and the name.
        ret = rpcRegister((char *)"getattr", argTypes, watdfs_getattr);
        if (ret < 0) {
            // It may be useful to have debug-printing here.
            DLOG("Register getattr fail");
            return ret;
        }
        DLOG("Register getattr success");
    }

    // register "fgetattr"
    {
        int argTypes[5];
        argTypes[1] = (1u << ARG_OUTPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[2] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[3] = (1u << ARG_OUTPUT) | (ARG_INT << 16u) ;
        argTypes[4] = 0;

        ret = rpcRegister((char *)"fgetattr", argTypes, watdfs_fgetattr);
        if(ret < 0){
            DLOG("Register fgetattr fail");
            return ret;
        }
        DLOG("Register fgetattr success");
    }

    // register "mknod"
    {
        // There are 4 args for the function (see watdfs_client.c for more
        // detail).

        int argTypes[5];
        // First is the path.
        argTypes[0] =
                (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        // The second argument is the mode.
        argTypes[1] =
                (1u << ARG_INPUT) | (ARG_INT << 16u) ;
        // The third argument is the dev.
        argTypes[2] =
                (1u << ARG_INPUT) | (ARG_LONG << 16u) ;
        // The fouth argument is the retcode.
        argTypes[3] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
        // Finally we fill in the null terminator.
        argTypes[4] = 0;

        // We need to register the function with the types and the name.
        ret = rpcRegister((char *)"mknod", argTypes, watdfs_mknod);
        if (ret < 0) {
            // It may be useful to have debug-printing here.
            DLOG("Register mknod fail");
            return ret;
        }
        DLOG("Register mknod success");
    }

    // register "open"
    {
        // There are 3 args for the function (see watdfs_client.c for more
        // detail).
        int argTypes[4];
        // First is the path.
        argTypes[0] =
                (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        // The second argument is the fi.
        argTypes[1] =
                (1u << ARG_INPUT) | (1u << ARG_OUTPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u ;
        // The third argument is the retcode.
        argTypes[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
        // Finally we fill in the null terminator.
        argTypes[3] = 0;

        ret = rpcRegister((char *)"open", argTypes, watdfs_open);
        if (ret < 0) {
            DLOG("Register open fail"); // register error
            return ret;
        }
        DLOG("Register open success");
    }

    // register "release"
    {
        int argTypes[4];
        argTypes[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[1] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
        argTypes[3] = 0;

        ret = rpcRegister((char *)"release", argTypes, watdfs_release);
        if(ret < 0){
            DLOG("Register release fail");
            return ret;
        }
        DLOG("Register release success");
    }

    // register "read"
    {
        int argTypes[7];
        argTypes[0] =
                (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[1] = (1u << ARG_OUTPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[2] = (1u << ARG_INPUT) | (ARG_LONG << 16u);
        argTypes[3] = (1u << ARG_INPUT) | (ARG_LONG << 16u) ;
        argTypes[4] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[5] = (1u << ARG_OUTPUT) | (ARG_INT << 16u) ;
        argTypes[6] = 0;

        ret = rpcRegister((char *)"read", argTypes, watdfs_read);
        if(ret < 0){
            DLOG("Register read fail");
            return ret;
        }
        DLOG("Register read success");
    }

    // register "write"
    {
        int argTypes[7];
        argTypes[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[1] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[2] = (1u << ARG_INPUT) | (ARG_LONG << 16u);
        argTypes[3] = (1u << ARG_INPUT) | (ARG_LONG << 16u) ;
        argTypes[4] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[5] = (1u << ARG_OUTPUT) | (ARG_INT << 16u) ;
        argTypes[6] = 0;

        ret = rpcRegister((char *)"write", argTypes, watdfs_write);
        if(ret < 0){
            DLOG("Register write fail");
            return ret;
        }
        DLOG("Register write success");

    }

    // register "truncate"
    {
        int argTypes[4];
        argTypes[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[1] = (1u << ARG_INPUT) |  (ARG_LONG << 16u);
        argTypes[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u) ;
        argTypes[3] = 0;

        ret = rpcRegister((char *)"truncate", argTypes, watdfs_truncate);
        if(ret < 0){
            DLOG("Register truncate fail");
            return ret;
        }
        DLOG("Register truncate success");

    }

    // register "utimens"
    {
        int argTypes[4];
        argTypes[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[1] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u) ;
        argTypes[3] = 0;

        ret = rpcRegister((char *)"utimens", argTypes, watdfs_utimens);
        if(ret < 0){
            DLOG("Register utimens fail");
            return ret;
        }
        DLOG("Register utimens success");

    }

    // register "fsync"
    {
        int argTypes[4];
        argTypes[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[1] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u) ;
        argTypes[3] = 0;

        ret = rpcRegister((char *)"fsync", argTypes, watdfs_utimens);
        if(ret < 0){
            DLOG("Register fsync fail");
            return ret;
        }
        DLOG("Register fsync success");

    }

    // TODO: Hand over control to the RPC library by calling `rpcExecute`.
    int rpcExc = rpcExecute();
    if(rpcExc == 0){
        DLOG("Executing server...");
    }
    else{
        DLOG("Executing server error...");
    }
    // rpcExecute could fail so you may want to have debug-printing here, and
    // then you should return.
    return ret;
}
