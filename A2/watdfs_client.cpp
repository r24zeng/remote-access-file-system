//
// Starter code for CS 454/654
// You SHOULD change this file
//

#include "watdfs_client.h"
#include "debug.h"
INIT_LOG

#include "rpc.h"

// SETUP AND TEARDOWN
void *watdfs_cli_init(struct fuse_conn_info *conn, const char *path_to_cache,
                      time_t cache_interval, int *ret_code) {
    // TODO: set up the RPC library by calling `rpcClientInit`.
    int rpcInit = rpcClientInit();
    if(rpcInit == 0){
        DLOG("Success init client !");
    }else{
        DLOG("Fail init client !");
    }

    // TODO: check the return code of the `rpcClientInit` call
    // `rpcClientInit` may fail, for example, if an incorrect port was exported.
    // It may be useful to print to stderr or stdout during debugging.
    // Important: Make sure you turn off logging prior to submission!
    // One useful technique is to use pre-processor flags like:
    // # ifdef PRINT_ERR
    // std::cerr << "Failed to initialize RPC Client" << std::endl;
    // #endif
    // Tip: Try using a macro for the above to minimize the debugging code.

    // TODO Initialize any global state that you require for the assignment and return it.
    // The value that you return here will be passed as userdata in other functions.
    // In A1, you might not need it, so you can return `nullptr`.
    void *userdata = nullptr;

    // TODO: save `path_to_cache` and `cache_interval` (for A3).

    // TODO: set `ret_code` to 0 if everything above succeeded else some appropriate
    // non-zero value.
    *ret_code = 0;
    // Return pointer to global state data.
    return userdata;
}

void watdfs_cli_destroy(void *userdata) {
    // TODO: clean up your userdata state.
    userdata = nullptr;
    // TODO: tear down the RPC library by calling `rpcClientDestroy`.
    int rpcDestroy = rpcClientDestroy();
    if(rpcDestroy == 0){
        DLOG("Success destroy client !");
    }else{
        DLOG("Fail destroy client !");
    }

}

// GET FILE ATTRIBUTES
int watdfs_cli_getattr(void *userdata, const char *path, struct stat *statbuf) {
    // SET UP THE RPC CALL
    DLOG("getattr is called from client...");

    // getattr has 3 arguments.
    int ARG_COUNT = 3;

    // Allocate space for the output arguments.
    void **args = (void **)malloc(ARG_COUNT * sizeof(void *));

    // Allocate the space for arg types, and one extra space for the null
    // array element.
    int arg_types[ARG_COUNT + 1];

    // The path has string length (strlen) + 1 (for the null character).
    int pathlen = strlen(path) + 1;

    // Fill in the arguments
    // The first argument is the path, it is an input only argument, and a char
    // array. The length of the array is the length of the path.
    arg_types[0] =
        (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) pathlen;
    // For arrays the argument is the array pointer, not a pointer to a pointer.
    args[0] = (void *)path;

    // The second argument is the stat structure. This argument is an output
    // only argument, and we treat it as a char array. The length of the array
    // is the size of the stat structure, which we can determine with sizeof.
    arg_types[1] = (1u << ARG_OUTPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) |
                   (uint) sizeof(struct stat); // statbuf
    args[1] = (void *)statbuf;

    // The third argument is the return code, an output only argument, which is
    // an integer.
    // TODO: fill in this argument type.

    arg_types[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u) ; // return code
    // The return code is not an array, so we need to hand args[2] an int*.
    // The int* could be the address of an integer located on the stack, or use
    // a heap allocated integer, in which case it should be freed.
    // TODO: Fill in the argument
    // get return code from server, args[2]
    int getattr_ret;
    args[2] = (void *) &getattr_ret;

    // Finally, the last position of the arg types is 0. There is no
    // corresponding arg.
    arg_types[3] = 0;

    // MAKE THE RPC CALL
    int rpc_ret = rpcCall((char *)"getattr", arg_types, args);

    // HANDLE THE RETURN
    // The integer value watdfs_cli_getattr will return.
    int fxn_ret = 0;
    if (rpc_ret < 0) {
        // Something went wrong with the rpcCall, return a sensible return
        // value. In this case lets return, -EINVAL
        DLOG( "rpcCall_getattr fail");
        fxn_ret = -EINVAL;
    } else {
        // Our RPC call succeeded. However, it's possible that the return code
        // from the server is not 0, that is it may be -errno. Therefore, we
        // should set our function return value to the retcode from the server.
        // TODO: set the function return value to the return code from the server.
        fxn_ret = getattr_ret;
    }

    if (fxn_ret < 0) {
        // Important: if the return code of watdfs_cli_getattr is negative (an
        // error), then we need to make sure that the stat structure is filled
        // with 0s. Otherwise, FUSE will be confused by the contradicting return
        // values.
        DLOG("Imply getattr on server fail");
        memset(statbuf, 0, sizeof(struct stat));
    }

    // Clean up the memory we have allocated.
    free(args);

    // Finally return the value we got from the server.
    DLOG("Return code from server : %d", fxn_ret);
    return fxn_ret;
}

int watdfs_cli_fgetattr(void *userdata, const char *path, struct stat *statbuf,
                        struct fuse_file_info *fi) {
    // At this point this method is not supported so it returns ENOSYS.
    DLOG("fgetattr is called from client...");
    int ARG_COUNT = 4;
    void **args = (void **)malloc(ARG_COUNT * sizeof(void *));
    int arg_types[ARG_COUNT + 1];
    int pathlen = strlen(path) + 1;

    arg_types[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) pathlen;
    arg_types[1] = (1u << ARG_OUTPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) sizeof(struct stat);
    arg_types[2] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) sizeof(struct fuse_file_info) ;
    arg_types[3] = (1u << ARG_OUTPUT) | (ARG_INT << 16u) ;
    arg_types[4] = 0;


    args[0] = (void *)path;
    args[1] = (void *) statbuf;
    args[2] = (void *)fi;
    int fgetattr_ret;
    args[3] = (void *) &fgetattr_ret;

    int rpc_ret = rpcCall((char *)"fgetattr", arg_types, args);
    int fxn_ret = 0;
    if (rpc_ret < 0) {
        DLOG( "rpcCall_fgetattr fail");
        fxn_ret = -EINVAL;
    } else {
        fxn_ret = fgetattr_ret;
    }

    if (fxn_ret < 0) {
        DLOG("Imply read on server fail");
    }

    free(args);
    DLOG("Return code from server : %d", fxn_ret);
    return fxn_ret;
}

// CREATE, OPEN AND CLOSE
int watdfs_cli_mknod(void *userdata, const char *path, mode_t mode, dev_t dev) {
    // Called to create a file.
    DLOG("mknod is called from client...");

    // set up RPC call
    // Allocate space for the output arguments.
    void **args = (void **)malloc(4 * sizeof(void *));

    // Allocate the space for arg types, and one extra space for the null
    // array element.
    int arg_types[5];

    // path is input, array and char type
    int pathlen = strlen(path) + 1;
    arg_types[0] =
            (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) pathlen;
    args[0] = (void *)path;

    // mode is input and int type
    arg_types[1] =
            (1u << ARG_INPUT) | (ARG_INT << 16u); // | (uint) sizeof(int);
    args[1] = (void *) &mode;

    // dev is input and long type
    arg_types[2] =
            (1u << ARG_INPUT) | (ARG_LONG << 16u);  //| (uint) sizeof(long);
    args[2] = (void *) &dev;

    // retcode is output and int type
    int mknod_ret;
    arg_types[3] = (1u << ARG_OUTPUT) | (ARG_INT << 16u); // | (uint) sizeof(int);
    args[3] = (void *) &mknod_ret;

    // Finally, the last position of the arg types is 0. There is no
    // corresponding arg.
    arg_types[4] = 0;

    // MAKE THE RPC CALL
    int rpc_ret = rpcCall((char *)"mknod", arg_types, args);

    // HANDLE THE RETURN
    // The integer value watdfs_cli_getattr will return.
    int fxn_ret = 0;
    if (rpc_ret < 0) {
        // Something went wrong with the rpcCall, return a sensible return
        // value. In this case lets return, -EINVAL
        DLOG("rpcCall_mknod fail ");
        fxn_ret = -EINVAL;
    } else {
        // Our RPC call succeeded. However, it's possible that the return code
        // from the server is not 0, that is it may be -errno. Therefore, we
        // should set our function return value to the retcode from the server.
        // TODO: set the function return value to the return code from the server.
        fxn_ret = mknod_ret;
    }

    if (fxn_ret < 0) {
        DLOG("Imply mknod in server fail");
    }

    // Clean up the memory we have allocated.
    free(args);

    // Finally return the value we got from the server.
    DLOG("Return code from server : %d", fxn_ret);
    return fxn_ret;

}

int watdfs_cli_open(void *userdata, const char *path,
                    struct fuse_file_info *fi) {
    // Called during open.
    // You should fill in fi->fh.
    DLOG("open is called from client ...");
    void **args = (void **)malloc(3 * sizeof(void *));
    int arg_types[4];
    int pathlen = strlen(path) + 1;

    // input path as array, char type
    arg_types[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) pathlen;
    args[0] = (void *)path;

    // fi is input and output, and is char array
    arg_types[1] = (1u << ARG_INPUT) | (1u << ARG_OUTPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) |
            (uint) sizeof(struct fuse_file_info);
    args[1] = (void *)fi;

    // retcode is output and int type, it means whether remote server call back success or not
    arg_types[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
    int open_ret;
    args[2] = (void *) &open_ret;

    // finally arg
    arg_types[3] = 0;

    // make rpc call
    int rpc_ret = rpcCall((char *)"open", arg_types, args);

    // handle error
    int fxn_ret = 0;
    if (rpc_ret < 0) { // rpc call open fail
        DLOG("rpcCall_open fail ");
        fxn_ret = -EINVAL;
    } else {
        fxn_ret = open_ret; // server fail
    }

    if (fxn_ret < 0) {
        DLOG("imply open in server fail");
    }

    // Clean up the memory we have allocated.
    free(args);
    // Finally return the value we got from the server.
    DLOG("Return code from server : %d", fxn_ret);
    return fxn_ret;
}

int watdfs_cli_release(void *userdata, const char *path,
                       struct fuse_file_info *fi) {
    // Called during close, but possibly asynchronously.
    DLOG("release is called from client ...");
    //*userdata = nullptr;
    void **args = (void **)malloc(3 * sizeof(void *));
    int arg_types[4];
    int pathlen = strlen(path) + 1;

    arg_types[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) pathlen;
    arg_types[1] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) sizeof(struct fuse_file_info);
    arg_types[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
    arg_types[3] = 0;
    args[0] = (void *)path;
    args[1] = (void *)fi;
    int release_ret;
    args[2] = (void *)&release_ret;

    int rpc_ret = rpcCall((char *)"release", arg_types, args);
    int fxn_ret = 0;
    if(rpc_ret < 0){
        DLOG("rpcCall_release fail");
        fxn_ret = -EINVAL;
    }
    else{
        fxn_ret = release_ret;
    }

    if(fxn_ret < 0){
        DLOG("imply release in server fail");

    }
    free(args);
    // Finally return the value we got from the server.
    DLOG("Return code from server : %d", fxn_ret);
    return fxn_ret;
}


// READ AND WRITE DATA
int watdfs_cli_read(void *userdata, const char *path, char *buf, size_t size,
                    off_t offset, struct fuse_file_info *fi) {
    // Read size amount of data at offset of file into buf.
    DLOG("read is called from client...");
    int ARG_COUNT = 6;
    void **args = (void **)malloc(ARG_COUNT * sizeof(void *));
    int arg_types[ARG_COUNT + 1];
    int pathlen = strlen(path) + 1;
    arg_types[0] =
            (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) pathlen;
    arg_types[2] = (1u << ARG_INPUT) | (ARG_LONG << 16u); // size
    arg_types[3] = (1u << ARG_INPUT) | (ARG_LONG << 16u); // offset
    arg_types[4] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) sizeof(struct fuse_file_info); // fi
    arg_types[5] = (1u << ARG_OUTPUT) | (ARG_INT << 16u); // ret
    arg_types[6] = 0;

    int rpc_ret, total_ret = 0, fxn_ret = 0;
    size_t size_remain = size;

    args[0] = (void *) path;
    args[4] = (void *) fi;

    while(size_remain > MAX_ARRAY_LEN){
        size = MAX_ARRAY_LEN;
        args[1] = (void *) buf;
        args[2] = (void *) &size;
        args[3] = (void *) &offset;
        int read_ret;
        args[5] = (void *) &read_ret;
        arg_types[1] = (1u << ARG_OUTPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) size; // change buf size
        DLOG("buf size is : %d", (uint)size);
        DLOG("buf size is : %ld", size);
        rpc_ret = rpcCall((char *)"read", arg_types, args);
        if(rpc_ret < 0){
            DLOG( "rpcCall_read fail");  // rpc call fail
            fxn_ret = -EINVAL;
            return fxn_ret;
        }
        else if(read_ret < 0){        // server fail
            DLOG("Imply read on server fail");
            fxn_ret = read_ret;
            return fxn_ret;
        }
        else if(read_ret < MAX_ARRAY_LEN){
            DLOG("finish read on client"); // finish read because can't read more from buf
            fxn_ret = total_ret + read_ret;
            return fxn_ret;
        }
        else{
            buf += read_ret; // update buf
            offset += read_ret; // update offset
            total_ret += read_ret; // update total read bytes
            size_remain -= MAX_ARRAY_LEN; // remain size need to read
        }
    }

    size = size_remain;
    args[1] = (void *) buf;
    args[2] = (void *) &size;
    args[3] = (void *) &offset;
    int read_ret;
    args[5] = (void *) &read_ret;
    arg_types[1] = (1u << ARG_OUTPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) size; // change buf size
    rpc_ret = rpcCall((char *)"read", arg_types, args);

    if(rpc_ret < 0){
        DLOG( "rpcCall_read fail");
        fxn_ret = -EINVAL;
    }
    else if(read_ret < 0){
        DLOG("Imply read on server fail");
        fxn_ret = read_ret;
    }
    else{
        fxn_ret = total_ret + read_ret; // update total read bytes
    }

    free(args);
    DLOG("Return code from server : %d", fxn_ret);
    return fxn_ret;
}

int watdfs_cli_write(void *userdata, const char *path, const char *buf,
                     size_t size, off_t offset, struct fuse_file_info *fi) {
    // Write size amount of data at offset of file from buf.

    // Remember that size may be greater then the maximum array size of the RPC
    // library.
    // Read size amount of data at offset of file into buf.
    DLOG("write is called from client...");
    int ARG_COUNT = 6;
    void **args = (void **)malloc(ARG_COUNT * sizeof(void *));
    int arg_types[ARG_COUNT + 1];
    int pathlen = strlen(path) + 1;

    arg_types[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) pathlen;
    arg_types[2] = (1u << ARG_INPUT) | (ARG_LONG << 16u);
    arg_types[3] = (1u << ARG_INPUT) | (ARG_LONG << 16u) ;
    arg_types[4] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) sizeof(struct fuse_file_info) ;
    arg_types[5] = (1u << ARG_OUTPUT) | (ARG_INT << 16u) ;
    arg_types[6] = 0;

    int rpc_ret, total_ret = 0, fxn_ret = 0;
    size_t size_remain = size;
    args[0] = (void *)path;
    args[4] = (void *)fi;


    while(size_remain > MAX_ARRAY_LEN){
        size = MAX_ARRAY_LEN;
        args[1] = (void *)buf;
        args[2] = (void *) &size;
        args[3] = (void *) &offset;
        int write_ret;
        args[5] = (void *) &write_ret;
        arg_types[1] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) size;
        rpc_ret = rpcCall((char *)"write", arg_types, args);

        if(rpc_ret < 0){
            DLOG( "rpcCall_read fail");  // rpc call fail
            fxn_ret = -EINVAL;
            return fxn_ret;
        }
        else if(write_ret < 0){        // server fail
            DLOG("Imply read on server fail");
            fxn_ret = write_ret;
            return fxn_ret;
        }
        else if(write_ret < MAX_ARRAY_LEN){
            DLOG("finish read on client"); // finish write because can't write more from buf
            fxn_ret = total_ret + write_ret;
            return fxn_ret;
        }
        else{
            buf += write_ret; // update buf
            offset += write_ret; // update offset
            total_ret += write_ret; // update total read bytes
            size_remain -= MAX_ARRAY_LEN; // remain size need to read
        }
    }

    size = size_remain;
    args[1] = (void *) buf;
    args[2] = (void *) &size;
    args[3] = (void *) &offset;
    int write_ret;
    args[5] = (void *) &write_ret;
    arg_types[1] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) size;
    rpc_ret = rpcCall((char *)"write", arg_types, args);

    if (rpc_ret < 0) {
        DLOG( "rpcCall_write fail");
        fxn_ret = -EINVAL;
    } else {
        fxn_ret = write_ret;
    }

    if (fxn_ret < 0) {
        DLOG("Imply write on server fail");
    }else{
        fxn_ret = total_ret + write_ret; // update total read bytes
    }

    free(args);
    DLOG("Return code from server : %d", fxn_ret);
    return fxn_ret;
}


int watdfs_cli_truncate(void *userdata, const char *path, off_t newsize) {
    // Change the file size to newsize.
    DLOG("truncate is called from client...");
    int ARG_COUNT = 3;
    void **args = (void **)malloc(ARG_COUNT * sizeof(void *));
    int arg_types[ARG_COUNT + 1];
    int pathlen = strlen(path) + 1;

    arg_types[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) pathlen;
    arg_types[1] = (1u << ARG_INPUT) |  (ARG_LONG << 16u);
    arg_types[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u) ;
    arg_types[3] = 0;

    args[0] = (void *)path;
    args[1] = (void *) &newsize;
    int truncate_ret;
    args[2] = (void *) &truncate_ret;

    int rpc_ret = rpcCall((char *)"truncate", arg_types, args);
    int fxn_ret = 0;
    if (rpc_ret < 0) {
        DLOG( "rpcCall_truncate fail");
        fxn_ret = -EINVAL;
    } else {
        fxn_ret = truncate_ret;
    }

    if (fxn_ret < 0) {
        DLOG("Imply truncate on server fail");
    }

    free(args);
    DLOG("Return code from server : %d", fxn_ret);
    return fxn_ret;
}


// SYNC A FILE TO STORAGE
int watdfs_cli_fsync(void *userdata, const char *path, struct fuse_file_info *fi) {
    // Force a flush of file data.
    // flushes modified file data and metadata from buffer to disk,
    // so that all changed information can be retrieved even if the system
    // crashes or is rebooted.
    DLOG("fsync is called from client...");
    int ARG_COUNT = 3;
    void **args = (void **)malloc(ARG_COUNT * sizeof(void *));
    int arg_types[ARG_COUNT + 1];
    int pathlen = strlen(path) + 1;

    arg_types[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) pathlen;
    arg_types[1] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) sizeof(struct fuse_file_info);
    arg_types[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u) ;
    arg_types[3] = 0;

    args[0] = (void *)path;
    args[1] = (void *) fi;
    int fsync_ret;
    args[2] = (void *) &fsync_ret;

    int rpc_ret = rpcCall((char *)"fsync", arg_types, args);
    int fxn_ret = 0;
    if (rpc_ret < 0) {
        DLOG( "rpcCall_fsync fail");
        fxn_ret = -EINVAL;
    } else {
        fxn_ret = fsync_ret;
    }

    if (fxn_ret < 0) {
        DLOG("Imply fsync on server fail");
    }

    free(args);
    DLOG("Return code from server : %d", fxn_ret);
    return fxn_ret;
}


// CHANGE METADATA
int watdfs_cli_utimens(void *userdata, const char *path,
                       const struct timespec ts[2]) {
    // Change file access and modification times.
    DLOG("utimens is called from client...");
    int ARG_COUNT = 3;
    void **args = (void **)malloc(ARG_COUNT * sizeof(void *));
    int arg_types[ARG_COUNT + 1];
    int pathlen = strlen(path) + 1;

    arg_types[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) pathlen;
    arg_types[1] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) 2*sizeof(struct timespec);
    arg_types[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u) ;
    arg_types[3] = 0;

    args[0] = (void *)path;
    args[1] = (void *) ts;
    int utimens_ret;
    args[2] = (void *) &utimens_ret;

    int rpc_ret = rpcCall((char *)"utimens", arg_types, args);
    int fxn_ret = 0;
    if (rpc_ret < 0) {
        DLOG( "rpcCall_utimens fail");
        fxn_ret = -EINVAL;
    } else {
        fxn_ret = utimens_ret;
    }

    if (fxn_ret < 0) {
        DLOG("Imply utimens on server fail");
    }

    free(args);
    DLOG("Return code from server : %d", fxn_ret);
    return fxn_ret;
}


