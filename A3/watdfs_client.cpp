//
// Starter code for CS 454/654
// You SHOULD change this file
//

#include "watdfs_client.h"
#include "rpcCall.h"
#include "debug.h"
INIT_LOG

#include "rpcCall.h"
#include "rpc.h"
#include <map>
#include <iostream>
#include <cstring>
#include <string>
#include <cstdint>


using namespace std;

// global variables
struct file_info{
    int flags;  // fi->flags
    int fh;  // fi->fh
    time_t tc;  // last cache entry time
};


struct global_v{
    char *path_to_cache;  // const client cache path
    time_t t;  // cache_interval
    // map<full_cache_path, file_info>, tc is last cache entry time
    std::map <string, struct file_info> open_file;
};

int get_flag(void *userdata, char *cache_path){
    struct global_v *user = (global_v *)userdata;
    string s_cache_path(cache_path);
    return (user->open_file)[s_cache_path].flags & O_ACCMODE;
}

int get_fh(void *userdata, char *cache_path){
    struct global_v *user = (global_v *)userdata;
    string s_cache_path(cache_path);
    int flag = (user->open_file)[s_cache_path].fh;
    return flag;
}

time_t get_tc(void *userdata, char *cache_path){
    struct global_v *user = (global_v *)userdata;
    string s_cache_path(cache_path);
    return (user->open_file)[s_cache_path].tc;
}

char *get_cache_path(void *userdata, const char *short_path) {
    struct global_v *user = (global_v *)userdata;
    int short_path_len = strlen(short_path);
    int dir_len = strlen(user->path_to_cache);
    int full_len = dir_len + short_path_len + 1;
    char *cache_path = (char *)malloc(full_len);

    // First fill in the directory.
    strcpy(cache_path, user->path_to_cache);
    // Then append the path.
    strcat(cache_path, short_path);
    DLOG("Full cache path: %s\n", cache_path);
    return cache_path;
}

void reset_tc(void *userdata, char *cache_path){
    struct global_v *user = (global_v *)userdata;
    string s_cache_path(cache_path);
    (user->open_file)[s_cache_path].tc = time(0);
}

void update_client_metadata(void *userdata, char *cache_path, struct timespec mtime){
    struct stat *stat_client = (struct stat *)malloc(sizeof(struct stat));
    int ret;
    DLOG("updata client's metadata");
    struct timespec t[2];
    t[0] = mtime;
    t[1] = mtime;
    ret = utimensat(0, cache_path, t, 0);
    ret = stat(cache_path, stat_client);

    free(stat_client);
}

void update_server_metadata(void *userdata, char *cache_path, const char *path, struct timespec mtime){
    struct stat *stat_server = (struct stat *)malloc(sizeof(struct stat));
    int ret;
    DLOG("updata server's metadata");
    struct timespec t[2];
    t[0] = mtime;
    t[1] = mtime;
    /* cout << "【Before call rpc_utimensat, ts[0]=" << t[0].tv_sec << "; ts[1]=" \
        << t[1].tv_sec << "】" << std::endl; */
    ret = rpc_utimens(userdata, path, t);

    ret = rpc_getattr(userdata, path, stat_server);
    //cout << "【after call rpc_utimensat, server_mtime=" << stat_server->st_mtime << " 】" << std::endl;
    free(stat_server);
}

int open_local_file(void *userdata, char *cache_path, int flags){
    string s_cache_path(cache_path);
    int ret;
    if(is_open(userdata, cache_path)){
        DLOG("file is open on client, can't open again");
        return -EMFILE;
    }
    ret = open(cache_path, flags);
    if(ret < 0){
        DLOG("open local file fail");
        return -errno;
    }else{
        struct global_v *user = (global_v *)userdata;
        (user->open_file)[s_cache_path].flags = flags;
        (user->open_file)[s_cache_path].fh = ret;
        (user->open_file)[s_cache_path].tc = time(0);
        DLOG("open file and update metadata on client success");
        return 0;
    }
}

int close_client(void *userdata, char *cache_path){
    string s_cache_path(cache_path);
    int ret;
    if(!is_open(userdata, cache_path)){
        DLOG("can't close file because it's not open");
        return -ENOENT;
    }

    ret = close(get_fh(userdata, cache_path));
    if(ret < 0){
        DLOG("close file on client fail");
        return -errno;
    }else{
        DLOG("close file on client success");
    }

    struct global_v *user = (global_v *)userdata;
    (user->open_file).erase(s_cache_path);
    return 0;
}

bool is_open(void *userdata, char *cache_path){
    struct global_v *user = (global_v *)userdata;
    string s_cache_path(cache_path);
    if((user->open_file).find(s_cache_path) != (user->open_file).end()){
        DLOG("now file is open");
        return true;
    }else{
        DLOG("now file is close");
        return false;
    }
}

bool is_fresh(void *userdata, const char *path){    // file is confirmed on both of client and server
    //cout << "*********** current time is:  " << time(0) << "  *************"<< std::endl;
    char *cache_path = get_cache_path(userdata, path);
    struct global_v *user = (global_v *)userdata;
    string s_cache_path(cache_path);
    int ret;
    if(time(0)-(user->open_file)[s_cache_path].tc < user->t){     // cache time is less then interval time, then it's fresh
        //cout << "【T - tc = " << time(0)-(user->open_file)[s_cache_path].tc << "】"<< std::endl;
        DLOG("【T - tc < t, fresh】");
        return true;
    }else{
        struct stat *stat_client = (struct stat *)malloc(sizeof(struct stat));
        struct stat *stat_server = (struct stat *)malloc(sizeof(struct stat));

        ret = rpc_getattr(userdata, path, stat_server);
        if(ret < 0){
            DLOG("get attr on server fail");
        }
        ret = stat(cache_path, stat_client);
        if(ret < 0){
            DLOG("get attr on client fail");
        }

        if(stat_client -> st_mtime == stat_server->st_mtime){ // modify is the same on both of client and server
            reset_tc(userdata, cache_path);
            free(stat_client);
            free(stat_server);
            DLOG("【T_server = T_client, fresh】");
            return true;
        }
    }
    DLOG("【The file is not fresh】");
    return false;
}


// ------------ rpc call lock and unlock --------------
int rpc_lock(const char *path, rw_lock_mode_t lock_mode){
    DLOG("lock is called from client...");
    int fxn_ret = 0;
    int ARG_COUNT=3;
    void **args = (void **)malloc(ARG_COUNT * sizeof(void *));
    int arg_types[ARG_COUNT + 1];
    int pathlen = strlen(path) + 1;

    arg_types[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) pathlen;
    arg_types[1] = (1u << ARG_INPUT) | (ARG_INT << 16u); // define read is 0, write is 1
    arg_types[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u); // retcode
    arg_types[3] = 0;

    args[0] = (void *)path;
    args[1] = (void *)&lock_mode;
    int lock_ret;
    args[2] = (void *)&lock_ret;

    int rpc_ret = rpcCall((char *)"lock", arg_types, args);
    if(rpc_ret < 0){
        DLOG("rpcCall_lock fail");
        fxn_ret = -EINVAL;
    }else{
        DLOG("Implement lock on server fails");
        fxn_ret = lock_ret;
    }

    free(args);
    DLOG("Return code from server : %d", fxn_ret);
    return fxn_ret;
}

int rpc_unlock(const char *path, rw_lock_mode_t unlock_mode){
    DLOG("lock is called from client...");
    int fxn_ret = 0;
    int ARG_COUNT=3;
    void **args = (void **)malloc(ARG_COUNT * sizeof(void *));
    int arg_types[ARG_COUNT + 1];
    int pathlen = strlen(path) + 1;

    arg_types[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) pathlen;
    arg_types[1] = (1u << ARG_INPUT) | (ARG_INT << 16u); // define read is 0, write is 1
    arg_types[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u); // retcode
    arg_types[3] = 0;

    args[0] = (void *)path;
    args[1] = (void *)&unlock_mode;
    int unlock_ret;
    args[2] = (void *)&unlock_ret;

    int rpc_ret = rpcCall((char *)"lock", arg_types, args);
    if(rpc_ret < 0){
        DLOG("rpcCall_lock fail");
        fxn_ret = -EINVAL;
    }else{
        DLOG("Implement lock on server fails");
        fxn_ret = unlock_ret;
    }

    free(args);
    DLOG("Return code from server : %d", fxn_ret);
    return fxn_ret;
}

// --------- transfer file from server to client ----------
// assume file may not exist on client, but must exist on server, at the end, close both
int watdfs_cli_download(void *userdata, const char *path){
    int ret, fxn_ret = 0;
    DLOG("start download file from server to client ......");
    struct stat *stat_server = (struct stat *)malloc(sizeof(struct stat));
    struct fuse_file_info *fi_server = (struct fuse_file_info *)malloc(sizeof(struct fuse_file_info));
    char *cache_path = get_cache_path(userdata, path);


    ret = rpc_getattr(userdata, path, stat_server);

    // open both
    int fh_cli = open(cache_path, O_RDWR);
    if(fh_cli < 0){
        DLOG("file doesn't exist on client, create new file");
        ret = mknod(cache_path, stat_server->st_mode, stat_server->st_dev);
        fh_cli = open(cache_path, O_RDWR);
    }


    // truncate file on client
    ret = truncate(cache_path, stat_server -> st_size);
    if(ret < 0){
        fxn_ret = -errno;
    }

    // read file from server to buffer
    char *buf = (char *)malloc((stat_server -> st_size)*sizeof(char));
    fi_server -> flags = O_RDONLY;
    ret = rpc_open(userdata, path, fi_server); // 只读方式打开不可能fail
    if(ret < 0){
        fxn_ret = ret;
    }
    ret = rpc_lock(path, RW_READ_LOCK);
    if(ret < 0){
        DLOG("get read lock fail");
        return ret;
    }else{
        DLOG("get read lock success");
    }
    ret = rpc_read(userdata, path, buf, stat_server -> st_size, 0, fi_server);
    DLOG("read from server to buf is : %s", buf);
    if(ret < 0){
        fxn_ret = ret;
        DLOG("fail read file on server");
    }

    // write from buffer to client
    ret = pwrite(fh_cli, buf, stat_server->st_size, 0);
    if(ret < 0){
        fxn_ret = -errno;
    }
    DLOG("write from buf to client is : %s", buf);
    //cout << "stat_server->st_size is ========" << stat_server->st_size;
    DLOG("write return code ======= %d", ret);

    // update client's metadata and close server
    update_client_metadata(userdata, cache_path, stat_server->st_mtim);
    ret = rpc_release(userdata, path, fi_server);
    if(ret < 0){
        fxn_ret = ret;
    }
    ret = close(fh_cli);
    if(ret < 0){
        fxn_ret = -errno;
    }

    if(fxn_ret < 0){
        DLOG("download file from server to client fail");
    }else{
        DLOG("download file from server to client success");
    }

    ret = rpc_unlock(path, RW_READ_LOCK);
    // free all heap
    free(stat_server);
    free(fi_server);
    free(cache_path);
    free(buf);
    return fxn_ret;
}

// --------- transfer file from client to server -----------
// assume the file exist but may not open on client, may not exist on server, at the end, close both
int watdfs_cli_upload(void *userdata, const char *path){
    int ret, fxn_ret=0;

    DLOG("start upload file from client to server ......");
    struct stat *stat_client = (struct stat *)malloc(sizeof(struct stat));
    struct stat *stat_server = (struct stat *)malloc(sizeof(struct stat));
    struct fuse_file_info *fi_server = (struct fuse_file_info *)malloc(sizeof(struct fuse_file_info));
    char *cache_path = get_cache_path(userdata, path);


    // get file attributes on client
    ret = stat(cache_path, stat_client);

    // open the responded file on server, if call upload function, fi_server->flag must can write
    // whether file exists on server
    fi_server -> flags = O_RDWR;
    ret = rpc_getattr(userdata, path, stat_server);
    if(ret < 0){
        ret = rpc_mknod(userdata, path, stat_client -> st_mode, stat_client -> st_dev);
        DLOG("file doesn't exist on server");
    }
    ret = rpc_open(userdata, path, fi_server);  // no write permission
    if(ret < 0){
        DLOG("no write permission during upload");
        // free all heap
        free(stat_client);
        free(stat_server);
        free(fi_server);
        free(cache_path);
        return ret;
    }

    ret = rpc_lock(path, RW_WRITE_LOCK);
    if(ret < 0){
        DLOG("get write lock fail");
        return ret;
    }else{
        DLOG("get write lock success");
    }
    
    // read file from client to buffer
    int fh_ret = open(cache_path, O_RDONLY);
    if(ret < 0){
        fxn_ret = -errno;
    }
    char *buf = (char *)malloc((stat_client->st_size)*sizeof(char));
    ret = pread(fh_ret, buf, stat_client->st_size, 0);
    DLOG("read from client to buf is : %s", buf);
    //cout << "stat_client->st_size is ========" << stat_client->st_size;
    DLOG("pread return code ======= %d", ret);
    if(ret < 0){
        fxn_ret = -errno;
    }

    // write from buffer to server
    ret = rpc_truncate(userdata, path, stat_client->st_size);
    if(ret < 0){
        fxn_ret = ret;
    }
    ret = rpc_write(userdata, path, buf, stat_client->st_size, 0, fi_server);
    if(ret < 0){
        fxn_ret = ret;
    }
    DLOG("write from buf to server is : %s", buf);
    update_server_metadata(userdata, cache_path, path, stat_client->st_mtim);

    if(fxn_ret < 0){
        DLOG("upload to server fail");
    }else{
        DLOG("download to client success");
    }

    ret = rpc_unlock(path, RW_WRITE_LOCK);
    // free all heap
    free(stat_client);
    free(stat_server);
    free(fi_server);
    free(cache_path);
    free(buf);
    return fxn_ret;
}

// ------------ client watdfs implement --------------
// SETUP AND TEARDOWN
void *watdfs_cli_init(struct fuse_conn_info *conn, const char *path_cache,
                      time_t cache_interval, int *ret_code) {
    // TODO: set up the RPC library by calling `rpcClientInit`.
    int rpcInit = rpcClientInit();
    if(rpcInit == 0){
        DLOG("Success init client !");
    }else{
        DLOG("Fail init client !");
    }

    // TODO: check the return code of the `rpcClientInit` call
    // `rpcClientInit` may fail, for example, if an incorrect port was exported.
    // It may be useful to print to stderr or stdout during debugging.
    // Important: Make sure you turn off logging prior to submission!
    // One useful technique is to use pre-processor flags like:
    // # ifdef PRINT_ERR
    // std::cerr << "Failed to initialize RPC Client" << std::endl;
    // #endif
    // Tip: Try using a macro for the above to minimize the debugging code.

    // TODO Initialize any global state that you require for the assignment and return it.
    // The value that you return here will be passed as userdata in other functions.
    // In A2, you might not need it, so you can return `nullptr`.
    struct global_v *user = new struct global_v;

    // TODO: save `path_to_cache` and `cache_interval` (for A3).
    user->path_to_cache = (char *)malloc(strlen(path_cache)+1);
    strcpy(user->path_to_cache, path_cache);
    user->t = cache_interval;

    // TODO: set `ret_code` to 0 if everything above succeeded else some appropriate
    // non-zero value.
    *ret_code = rpcInit;
    // Return pointer to global state data.
    return (void *)user;
}


void watdfs_cli_destroy(void *userdata) {
    struct global_v *user = (global_v *)userdata;
    // TODO: tear down the RPC library by calling `rpcClientDestroy`.
    int rpcDestroy = rpcClientDestroy();
    if(rpcDestroy == 0){
        free(user->path_to_cache);
        // TODO: clean up your userdata state.
        userdata = nullptr;
        DLOG("Success destroy client !");
    }else{
        DLOG("Fail destroy client !");
    }
}

// *************** get file attributes，check open stat -> check freshness -> local operate
int watdfs_cli_getattr(void *userdata, const char *path, struct stat *statbuf){
    struct stat *stat_server = (struct stat *)malloc(sizeof(struct stat));
    int ret, fxn_ret = 0;
    char *cache_path = get_cache_path(userdata, path);

    // check open
    if(!is_open(userdata, cache_path)) {   // if file doesn't open, then transfer file from server to client
        ret = rpc_getattr(userdata, path, stat_server);
        if(ret < 0){   // don't exist on server
            DLOG("file doesn't on server");
            fxn_ret = ret;
        }else{
            ret = watdfs_cli_download(userdata, path);
            DLOG("watdfs_cli_download return code ===== %d", ret);
            int fh_ret = open(cache_path, O_RDONLY);
            ret = stat(cache_path, statbuf);  // 文件已在本地存在，所以不会失败
            ret = close(fh_ret);
            if(ret < 0){
                fxn_ret = -errno;
            }
        }
    }else {
        // check file flag
        // file open in read mode must check fresh, in write mode can't check fresh
        if (get_flag(userdata, cache_path) == O_RDONLY) {
            // check freshness
            if (!is_fresh(userdata, path)) {
                ret = watdfs_cli_download(userdata, path); // 不可能失败，因为文件一定存在，并且以在server上以只读的形式打开
                // 已经打开的文件，fh没有从open_file里删掉，也没有可以关闭文件，相当于没有关闭，因此没必要重新打开
                DLOG("watdfs_cli_download return code ===== %d", ret);
                if(ret < 0){
                    fxn_ret = ret;
                }else{
                    reset_tc(userdata, cache_path);
                }
            }
        }
        ret = stat(cache_path, statbuf);
        DLOG("stat return code ===== %d", ret);
        if(ret < 0){
            fxn_ret = -errno;
        }
    }

    if(fxn_ret < 0){
        DLOG("watdfs_cli_getattr fail");
        memset(statbuf, 0, sizeof(struct stat));
    }

    free(stat_server);
    free(cache_path);
    return fxn_ret;
}

int rpc_getattr(void *userdata, const char *path, struct stat *statbuf) {
    // SET UP THE RPC CALL
    DLOG("getattr is called from client...");

    // getattr has 3 arguments.
    int ARG_COUNT = 3;

    // Allocate space for the output arguments.
    void **args = (void **)malloc(ARG_COUNT * sizeof(void *));

    // Allocate the space for arg types, and one extra space for the null
    // array element.
    int arg_types[ARG_COUNT + 1];

    // The path has string length (strlen) + 1 (for the null character).
    int pathlen = strlen(path) + 1;

    // Fill in the arguments
    // The first argument is the path, it is an input only argument, and a char
    // array. The length of the array is the length of the path.
    arg_types[0] =
        (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) pathlen;
    // For arrays the argument is the array pointer, not a pointer to a pointer.
    args[0] = (void *)path;

    // The second argument is the stat structure. This argument is an output
    // only argument, and we treat it as a char array. The length of the array
    // is the size of the stat structure, which we can determine with sizeof.
    arg_types[1] = (1u << ARG_OUTPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) |
                   (uint) sizeof(struct stat); // statbuf
    args[1] = (void *)statbuf;

    // The third argument is the return code, an output only argument, which is
    // an integer.
    // TODO: fill in this argument type.

    arg_types[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u) ; // return code
    // The return code is not an array, so we need to hand args[2] an int*.
    // The int* could be the address of an integer located on the stack, or use
    // a heap allocated integer, in which case it should be freed.
    // TODO: Fill in the argument
    // get return code from server, args[2]
    int getattr_ret;
    args[2] = (void *) &getattr_ret;

    // Finally, the last position of the arg types is 0. There is no
    // corresponding arg.
    arg_types[3] = 0;

    // MAKE THE RPC CALL
    int rpc_ret = rpcCall((char *)"getattr", arg_types, args);

    // HANDLE THE RETURN
    // The integer value watdfs_cli_getattr will return.
    int fxn_ret = 0;
    if (rpc_ret < 0) {
        // Something went wrong with the rpcCall, return a sensible return
        // value. In this case lets return, -EINVAL
        DLOG( "rpcCall_getattr fail");
        fxn_ret = -EINVAL;
    } else {
        // Our RPC call succeeded. However, it's possible that the return code
        // from the server is not 0, that is it may be -errno. Therefore, we
        // should set our function return value to the retcode from the server.
        // TODO: set the function return value to the return code from the server.
        fxn_ret = getattr_ret;
    }

    if (fxn_ret < 0) {
        // Important: if the return code of watdfs_cli_getattr is negative (an
        // error), then we need to make sure that the stat structure is filled
        // with 0s. Otherwise, FUSE will be confused by the contradicting return
        // values.
        DLOG("Imply getattr on server fail");
        memset(statbuf, 0, sizeof(struct stat));
    }

    // Clean up the memory we have allocated.
    free(args);

    // Finally return the value we got from the server.
    DLOG("Return code from server : %d", fxn_ret);
    return fxn_ret;
}


// *************** get file attributes by fi，check open stat -> check freshness -> local operate
int watdfs_cli_fgetattr(void *userdata, const char *path, struct stat *statbuf, struct fuse_file_info *fi){
    struct stat *stat_server = (struct stat *)malloc(sizeof(struct stat));
    int ret, fxn_ret = 0;
    char *cache_path = get_cache_path(userdata, path);

    // check open
    if(!is_open(userdata, cache_path)) {   // if file doesn't open, then transfer file from server to client
        ret = rpc_fgetattr(userdata, path, stat_server, fi);
        if(ret < 0){
            fxn_ret = ret;  // don't exist on server
        }else{
            ret = watdfs_cli_download(userdata, path);
            int fh_ret = open(cache_path, O_RDONLY);
            ret = fstat(fh_ret, statbuf);  // 文件已在本地存在，所以不会失败
            ret = close(fh_ret);
            if(ret < 0){
                fxn_ret = -errno;
            }
        }
    }else{
        // check file flag
        // file open in read mode must check fresh, in write mode can't check fresh
        if(get_flag(userdata, cache_path) == O_RDONLY){
            // check freshness
            if(!is_fresh(userdata, path)){
                ret = watdfs_cli_download(userdata, path);  // 不可能失败，因为文件一定存在并且以只读方式在server上打开
                DLOG("watdfs_cli_download return code ===== %d", ret);
                if(ret < 0){
                    fxn_ret = ret;
                }else{
                    reset_tc(userdata, cache_path);
                }
            }
        }
        // under both read and write mode
        DLOG("fstat start ............");
        ret = fstat(get_fh(userdata, cache_path), statbuf);
        DLOG("fstat result ........... return code: %d", ret);
        if(ret < 0) {
            fxn_ret = -errno;
        }
    }

    if(fxn_ret < 0){
        DLOG("watdfs_cli_fgetattr fail");
        memset(statbuf, 0, sizeof(struct stat));
    }

    free(stat_server);
    free(cache_path);
    return fxn_ret;
}

int rpc_fgetattr(void *userdata, const char *path, struct stat *statbuf,
                        struct fuse_file_info *fi) {
    // At this point this method is not supported so it returns ENOSYS.
    DLOG("fgetattr is called from client...");
    int ARG_COUNT = 4;
    void **args = (void **)malloc(ARG_COUNT * sizeof(void *));
    int arg_types[ARG_COUNT + 1];
    int pathlen = strlen(path) + 1;

    arg_types[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) pathlen;
    arg_types[1] = (1u << ARG_OUTPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) sizeof(struct stat);
    arg_types[2] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) sizeof(struct fuse_file_info) ;
    arg_types[3] = (1u << ARG_OUTPUT) | (ARG_INT << 16u) ;
    arg_types[4] = 0;


    args[0] = (void *)path;
    args[1] = (void *) statbuf;
    args[2] = (void *)fi;
    int fgetattr_ret;
    args[3] = (void *) &fgetattr_ret;

    int rpc_ret = rpcCall((char *)"fgetattr", arg_types, args);
    int fxn_ret = 0;
    if (rpc_ret < 0) {
        DLOG( "rpcCall_fgetattr fail");
        fxn_ret = -EINVAL;
    } else {
        fxn_ret = fgetattr_ret;
    }

    if (fxn_ret < 0) {
        DLOG("Imply read on server fail");
        memset(statbuf, 0, sizeof(struct stat));
    }

    free(args);
    DLOG("Return code from server : %d", fxn_ret);
    return fxn_ret;
}

// *************** create file，check open -> mknod local -> transfer to server (close both)
int watdfs_cli_mknod(void *userdata, const char *path, mode_t mode, dev_t dev){
    char *cache_path = get_cache_path(userdata, path);
    int ret, fxn_ret = 0;

    //check open
    if(!is_open(userdata, cache_path)){
        ret = mknod(cache_path, mode, dev);  // 在本地存在，那么在server上肯定存在，一定失败
        if(ret < 0){
            DLOG("can't create file, file exist on client");
            fxn_ret = -errno;
        }else{    // 在本地不存在，可能在server上存在，所以可能也会失败，这个判断写在upload里面了
            // 所以应该先用rpc_getattr测一下，我这里需要待验证mknod的功能后再补充
            ret = watdfs_cli_upload(userdata, path);
            DLOG("watdfs_cli_upload return code ===== %d", ret);
            if(ret < 0){
                fxn_ret = ret;   // no write permission on server
            }
        }
    }else{
        // check open mode
        if(get_flag(userdata, cache_path) == O_RDONLY){
            DLOG("can't do mknod under read mode");
            fxn_ret = -EMFILE;
        }else{   // 测试用，实际上在文件打开的状态下一定会失败，？如果打开了，直接返回错误
            ret = mknod(cache_path, mode, dev);
            if(ret < 0){
                DLOG("can't create file, file exist on client");
                fxn_ret = -errno;
            }else{
                if(!is_fresh(userdata, path)){
                    ret = watdfs_cli_upload(userdata, path);
                    DLOG("watdfs_cli_upload return code ===== %d", ret);
                    if(ret < 0){
                        fxn_ret = ret;
                    }else{
                        reset_tc(userdata, cache_path);
                    }
                }
            }
        }
    }

    if(fxn_ret < 0){
        DLOG("watdfs_cli_mknod fail");
    }
    free(cache_path);
    return fxn_ret;
}
// CREATE, OPEN AND CLOSE
int rpc_mknod(void *userdata, const char *path, mode_t mode, dev_t dev) {
    // Called to create a file.
    DLOG("mknod is called from client...");

    // set up RPC call
    // Allocate space for the output arguments.
    void **args = (void **)malloc(4 * sizeof(void *));

    // Allocate the space for arg types, and one extra space for the null
    // array element.
    int arg_types[5];

    // path is input, array and char type
    int pathlen = strlen(path) + 1;
    arg_types[0] =
            (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) pathlen;
    args[0] = (void *)path;

    // mode is input and int type
    arg_types[1] =
            (1u << ARG_INPUT) | (ARG_INT << 16u); // | (uint) sizeof(int);
    args[1] = (void *) &mode;

    // dev is input and long type
    arg_types[2] =
            (1u << ARG_INPUT) | (ARG_LONG << 16u);  //| (uint) sizeof(long);
    args[2] = (void *) &dev;

    // retcode is output and int type
    int mknod_ret;
    arg_types[3] = (1u << ARG_OUTPUT) | (ARG_INT << 16u); // | (uint) sizeof(int);
    args[3] = (void *) &mknod_ret;

    // Finally, the last position of the arg types is 0. There is no
    // corresponding arg.
    arg_types[4] = 0;

    // MAKE THE RPC CALL
    int rpc_ret = rpcCall((char *)"mknod", arg_types, args);

    // HANDLE THE RETURN
    // The integer value watdfs_cli_getattr will return.
    int fxn_ret = 0;
    if (rpc_ret < 0) {
        // Something went wrong with the rpcCall, return a sensible return
        // value. In this case lets return, -EINVAL
        DLOG("rpcCall_mknod fail ");
        fxn_ret = -EINVAL;
    } else {
        // Our RPC call succeeded. However, it's possible that the return code
        // from the server is not 0, that is it may be -errno. Therefore, we
        // should set our function return value to the retcode from the server.
        // TODO: set the function return value to the return code from the server.
        fxn_ret = mknod_ret;
    }

    if (fxn_ret < 0) {
        DLOG("Imply mknod in server fail");
    }

    // Clean up the memory we have allocated.
    free(args);

    // Finally return the value we got from the server.
    DLOG("Return code from server : %d", fxn_ret);
    return fxn_ret;
}


// *************** open file，check open stat -> transfer from server to client or open again error
// open flag conflict should be represented on server
int watdfs_cli_open(void *userdata, const char *path, struct fuse_file_info *fi){
    struct stat *stat_server = (struct stat *)malloc(sizeof(struct stat));
    char *cache_path = get_cache_path(userdata, path);
    int ret, fxn_ret = 0;

    //check open
    if(is_open(userdata, cache_path)){
        fxn_ret = -EMFILE;
    }else{
        // check whether file exists on server
        ret = rpc_getattr(userdata, path, stat_server);
        if(ret < 0) {  // 文件在远端不存在
            DLOG("file doesn't exist on server");
            if(fi->flags != O_CREAT){     // 怎么查如果是O_CREAT | O_RD
                fxn_ret = ret;
                DLOG("don't allow to create a new file on server");
            }else{
                ret = rpc_open(userdata, path, fi);  // 远端既然不存在，用open新建不会报错
                if(ret < 0){
                    fxn_ret = ret;
                }
                ret = watdfs_cli_download(userdata, path);  // 已经打开了远端server，不能再一次打开（写操作矛盾），顺便打开本地
                if(ret < 0){
                    fxn_ret = ret;
                }else{
                    DLOG("create a new file and download from server");
                }
            }
        }else {
            ret = watdfs_cli_download(userdata, path);  // 打开本地文件
            DLOG("watdfs_cli_download return code ===== %d", ret);
            if(ret < 0){
                fxn_ret = ret;
            }
        }
    }

    if(fxn_ret < 0){
        free(stat_server);
        free(cache_path);
        return fxn_ret;
    }else{
        ret = open_local_file(userdata, cache_path, fi->flags);
        if(ret < 0) {
            fxn_ret = ret;
        }
        free(stat_server);
        free(cache_path);
        return fxn_ret;
    }
}

int rpc_open(void *userdata, const char *path,
                    struct fuse_file_info *fi) {
    // Called during open.
    // You should fill in fi->fh.
    DLOG("open is called from client ...");
    void **args = (void **)malloc(3 * sizeof(void *));
    int arg_types[4];
    int pathlen = strlen(path) + 1;

    // input path as array, char type
    arg_types[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) pathlen;
    args[0] = (void *)path;

    // fi is input and output, and is char array
    arg_types[1] = (1u << ARG_INPUT) | (1u << ARG_OUTPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) |
            (uint) sizeof(struct fuse_file_info);
    args[1] = (void *)fi;

    // retcode is output and int type, it means whether remote server call back success or not
    arg_types[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
    int open_ret;
    args[2] = (void *) &open_ret;

    // finally arg
    arg_types[3] = 0;

    // make rpc call
    int rpc_ret = rpcCall((char *)"open", arg_types, args);

    // handle error
    int fxn_ret = 0;
    if (rpc_ret < 0) { // rpc call open fail
        DLOG("rpcCall_open fail ");
        fxn_ret = -EINVAL;
    } else {
        fxn_ret = open_ret; // server fail
    }

    if (fxn_ret < 0) {
        DLOG("imply open in server fail");
    }

    // Clean up the memory we have allocated.
    free(args);
    // Finally return the value we got from the server.
    DLOG("Return code from server : %d", fxn_ret);
    return fxn_ret;
}

// *************** close file，transfer file from client to server  -> close both -> remove open stat
int watdfs_cli_release(void *userdata, const char *path, struct fuse_file_info *fi){
    DLOG("start release from client");
    char *cache_path = get_cache_path(userdata, path);
    int ret, fxn_ret = 0;

    // check mode
    if(get_flag(userdata, cache_path) != O_RDONLY){
        ret = watdfs_cli_upload(userdata, path);
        if(ret < 0){
            fxn_ret = ret;
        }
    }

    // close file on client, but not release on server, because it's not open on server 别忘了解开互斥锁
    ret = close_client(userdata, cache_path);
    free(cache_path);
    return fxn_ret;
}

int rpc_release(void *userdata, const char *path,
                       struct fuse_file_info *fi) {
    // Called during close, but possibly asynchronously.
    DLOG("release is called from client ...");
    //*userdata = nullptr;
    void **args = (void **)malloc(3 * sizeof(void *));
    int arg_types[4];
    int pathlen = strlen(path) + 1;

    arg_types[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) pathlen;
    arg_types[1] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) sizeof(struct fuse_file_info);
    arg_types[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
    arg_types[3] = 0;
    args[0] = (void *)path;
    args[1] = (void *)fi;
    int release_ret;
    args[2] = (void *)&release_ret;

    int rpc_ret = rpcCall((char *)"release", arg_types, args);
    int fxn_ret = 0;
    if(rpc_ret < 0){
        DLOG("rpcCall_release fail");
        fxn_ret = -EINVAL;
    }
    else{
        fxn_ret = release_ret;
    }

    if(fxn_ret < 0){
        DLOG("imply release in server fail");

    }
    free(args);
    // Finally return the value we got from the server.
    DLOG("Return code from server : %d", fxn_ret);
    return fxn_ret;
}


// -------------------------- READ AND WRITE DATA
int watdfs_cli_read(void *userdata, const char *path, char *buf, size_t size,
                    off_t offset, struct fuse_file_info *fi){
    char *cache_path = get_cache_path(userdata, path);
    int ret, read_ret = 0, fxn_ret = 0;

    // check open
    if(!is_open(userdata, cache_path)){
        DLOG("can't read before open");
        fxn_ret = -EPERM;  // no permission to do read without open
    }else{  // if it's open, then it must exist on server
        // check flag
        if(get_flag(userdata, cache_path) == O_RDONLY){
            // check fresh
            if(!is_fresh(userdata, path)){
                ret = watdfs_cli_download(userdata, path);  // 不可能失败
                reset_tc(userdata, cache_path);
            }
        }
    }

    // operate locally
    if(fxn_ret < 0){
        free(cache_path);
        DLOG("wadfs_cli_read fail");
        return fxn_ret;
    }else{
        read_ret = pread(get_fh(userdata, cache_path), buf, size, offset);
        DLOG("read %d characters", read_ret);
        DLOG("read to buf in watdfs_cli_read is %s", buf);
        DLOG("watdfs_cli_read success");
        return read_ret;
    }
}

int rpc_read(void *userdata, const char *path, char *buf, size_t size,
                    off_t offset, struct fuse_file_info *fi) {
    // Read size amount of data at offset of file into buf.
    DLOG("read is called from client...");
    int ARG_COUNT = 6;
    void **args = (void **)malloc(ARG_COUNT * sizeof(void *));
    int arg_types[ARG_COUNT + 1];
    int pathlen = strlen(path) + 1;
    arg_types[0] =
            (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) pathlen;
    arg_types[2] = (1u << ARG_INPUT) | (ARG_LONG << 16u); // size
    arg_types[3] = (1u << ARG_INPUT) | (ARG_LONG << 16u); // offset
    arg_types[4] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) sizeof(struct fuse_file_info); // fi
    arg_types[5] = (1u << ARG_OUTPUT) | (ARG_INT << 16u); // ret
    arg_types[6] = 0;

    int rpc_ret, total_ret = 0, fxn_ret = 0;
    size_t size_remain = size;

    args[0] = (void *) path;
    args[4] = (void *) fi;

    while(size_remain > MAX_ARRAY_LEN){
        size = MAX_ARRAY_LEN;
        args[1] = (void *) buf;
        args[2] = (void *) &size;
        args[3] = (void *) &offset;
        int read_ret;
        args[5] = (void *) &read_ret;
        arg_types[1] = (1u << ARG_OUTPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) size; // change buf size
        DLOG("buf size is : %d", (uint)size);
        DLOG("buf size is : %ld", size);
        rpc_ret = rpcCall((char *)"read", arg_types, args);
        if(rpc_ret < 0){
            DLOG( "rpcCall_read fail");  // rpc call fail
            fxn_ret = -EINVAL;
            return fxn_ret;
        }
        else if(read_ret < 0){        // server fail
            DLOG("Imply read on server fail");
            fxn_ret = read_ret;
            return fxn_ret;
        }
        else if(read_ret < MAX_ARRAY_LEN){
            DLOG("finish read on client"); // finish read because can't read more from buf
            fxn_ret = total_ret + read_ret;
            return fxn_ret;
        }
        else{
            buf += read_ret; // update buf
            offset += read_ret; // update offset
            total_ret += read_ret; // update total read bytes
            size_remain -= MAX_ARRAY_LEN; // remain size need to read
        }
    }

    size = size_remain;
    args[1] = (void *) buf;
    args[2] = (void *) &size;
    args[3] = (void *) &offset;
    int read_ret;
    args[5] = (void *) &read_ret;
    arg_types[1] = (1u << ARG_OUTPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) size; // change buf size
    rpc_ret = rpcCall((char *)"read", arg_types, args);

    if(rpc_ret < 0){
        DLOG( "rpcCall_read fail");
        fxn_ret = -EINVAL;
    }
    else if(read_ret < 0){
        DLOG("Imply read on server fail");
        fxn_ret = read_ret;
    }
    else{
        fxn_ret = total_ret + read_ret; // update total read bytes
    }

    free(args);
    DLOG("Return code from server : %d", fxn_ret);
    return fxn_ret;
}


int watdfs_cli_write(void *userdata, const char *path, const char *buf,
                     size_t size, off_t offset, struct fuse_file_info *fi){
    // watdfs_cli_open has been called before this call
    // file has been opened on client
    // open file on server
    struct stat *stat_server = (struct stat *)malloc(sizeof(struct stat));
    char *cache_path = get_cache_path(userdata, path);
    int ret, write_ret = 0, fxn_ret = 0;

    // check open
    if(!is_open(userdata, cache_path)){
        return -EPERM;
    }else{
        if(get_flag(userdata, cache_path) == O_RDONLY){
            fxn_ret = -EMFILE;
        }else{
            write_ret = pwrite(get_fh(userdata, cache_path), buf, size, offset);
            DLOG("write %d characters", write_ret);
            DLOG("write to buf in watdfs_cli_write is %s", buf);
            if(write_ret < 0){
                DLOG("watdfs_cli_write fail");
                fxn_ret = -errno;
            }else{
                DLOG("write from buf to client success, start to check fresh");
                if(!is_fresh(userdata, path)){
                    ret = watdfs_cli_upload(userdata, path);
                    reset_tc(userdata, cache_path);
                    if(ret < 0){
                        fxn_ret = ret;
                    }
                }
            }
        }
    }

    free(stat_server);
    free(cache_path);
    if(fxn_ret < 0){
        return fxn_ret;
    }else{
        return write_ret;
    }
}
int rpc_write(void *userdata, const char *path, const char *buf,
                     size_t size, off_t offset, struct fuse_file_info *fi) {
    // Write size amount of data at offset of file from buf.

    // Remember that size may be greater then the maximum array size of the RPC
    // library.
    // Read size amount of data at offset of file into buf.
    DLOG("write is called from client...");
    int ARG_COUNT = 6;
    void **args = (void **)malloc(ARG_COUNT * sizeof(void *));
    int arg_types[ARG_COUNT + 1];
    int pathlen = strlen(path) + 1;

    arg_types[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) pathlen;
    arg_types[2] = (1u << ARG_INPUT) | (ARG_LONG << 16u);
    arg_types[3] = (1u << ARG_INPUT) | (ARG_LONG << 16u) ;
    arg_types[4] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) sizeof(struct fuse_file_info) ;
    arg_types[5] = (1u << ARG_OUTPUT) | (ARG_INT << 16u) ;
    arg_types[6] = 0;

    int rpc_ret, total_ret = 0, fxn_ret = 0;
    size_t size_remain = size;
    args[0] = (void *)path;
    args[4] = (void *)fi;


    while(size_remain > MAX_ARRAY_LEN){
        size = MAX_ARRAY_LEN;
        args[1] = (void *)buf;
        args[2] = (void *) &size;
        args[3] = (void *) &offset;
        int write_ret;
        args[5] = (void *) &write_ret;
        arg_types[1] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) size;
        rpc_ret = rpcCall((char *)"write", arg_types, args);

        if(rpc_ret < 0){
            DLOG( "rpcCall_read fail");  // rpc call fail
            fxn_ret = -EINVAL;
            return fxn_ret;
        }
        else if(write_ret < 0){        // server fail
            DLOG("Imply read on server fail");
            fxn_ret = write_ret;
            return fxn_ret;
        }
        else if(write_ret < MAX_ARRAY_LEN){
            DLOG("finish read on client"); // finish write because can't write more from buf
            fxn_ret = total_ret + write_ret;
            return fxn_ret;
        }
        else{
            buf += write_ret; // update buf
            offset += write_ret; // update offset
            total_ret += write_ret; // update total read bytes
            size_remain -= MAX_ARRAY_LEN; // remain size need to read
        }
    }

    size = size_remain;
    args[1] = (void *) buf;
    args[2] = (void *) &size;
    args[3] = (void *) &offset;
    int write_ret;
    args[5] = (void *) &write_ret;
    arg_types[1] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) size;
    rpc_ret = rpcCall((char *)"write", arg_types, args);

    if (rpc_ret < 0) {
        DLOG( "rpcCall_write fail");
        fxn_ret = -EINVAL;
    } else {
        fxn_ret = write_ret;
    }

    if (fxn_ret < 0) {
        DLOG("Imply write on server fail");
    }else{
        fxn_ret = total_ret + write_ret; // update total read bytes
    }

    free(args);
    DLOG("Return code from server : %d", fxn_ret);
    return fxn_ret;
}

int watdfs_cli_truncate(void *userdata, const char *path, off_t newsize){
    struct stat *stat_server = (struct stat *)malloc(sizeof(struct stat));
    char *cache_path = get_cache_path(userdata, path);
    int ret, fxn_ret = 0;

    // check open
    if(!is_open(userdata, cache_path)){
        ret = rpc_getattr(userdata, path, stat_server);
        if(ret < 0){  // don't exist on server
            fxn_ret = ret;
            DLOG("download during truncate fail, error code is: %d", fxn_ret);
        }else{
            ret = watdfs_cli_download(userdata, path);  // 一定成功，因为在server上文件存在并以只读的方式的开
            int fh_ret = open(cache_path, O_RDWR);
            ret = truncate(cache_path, newsize);
            if(ret < 0) {
                DLOG("truncate on client fail");
                fxn_ret = -errno;
            }
            ret = close(fh_ret);
        }
    }else{
        if(get_flag(userdata, cache_path) == O_RDONLY){
            DLOG("read only mode can't do truncate, return imediatelly");
            free(stat_server);
            free(cache_path);
            return -EACCES;
        }
        ret = truncate(cache_path, newsize);
        if(ret < 0){
            DLOG("truncate on client fail");
            fxn_ret = -errno;
        }else{
            if(!is_fresh(userdata, path)){
                ret = watdfs_cli_upload(userdata, path);  // upload后自然就关掉了本地文件
                reset_tc(userdata, cache_path);
                if(ret < 0){   // server端没有写权限
                    fxn_ret = ret;
                    DLOG("upload during truncate fail, error code is: %d", fxn_ret);
                }
            }
        }
    }

    free(cache_path);
    return fxn_ret;
}

int rpc_truncate(void *userdata, const char *path, off_t newsize) {
    // Change the file size to newsize.
    DLOG("truncate is called from client...");
    int ARG_COUNT = 3;
    void **args = (void **)malloc(ARG_COUNT * sizeof(void *));
    int arg_types[ARG_COUNT + 1];
    int pathlen = strlen(path) + 1;

    arg_types[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) pathlen;
    arg_types[1] = (1u << ARG_INPUT) |  (ARG_LONG << 16u);
    arg_types[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u) ;
    arg_types[3] = 0;

    args[0] = (void *)path;
    args[1] = (void *) &newsize;
    int truncate_ret;
    args[2] = (void *) &truncate_ret;

    int rpc_ret = rpcCall((char *)"truncate", arg_types, args);
    int fxn_ret = 0;
    if (rpc_ret < 0) {
        DLOG( "rpcCall_truncate fail");
        fxn_ret = -EINVAL;
    } else {
        fxn_ret = truncate_ret;
    }

    if (fxn_ret < 0) {
        DLOG("Imply truncate on server fail");
    }

    free(args);
    DLOG("Return code from server : %d", fxn_ret);
    return fxn_ret;
}


// --------------------------------- SYNC A FILE TO STORAGE
int watdfs_cli_fsync(void *userdata, const char *path, struct fuse_file_info *fi){
    int ret;
    char *cache_path = get_cache_path(userdata, path);
    string s_cache_path(cache_path);
    ret = watdfs_cli_upload(userdata, path);
    if(ret < 0){
        free(cache_path);
        return ret;
    }

    reset_tc(userdata, cache_path);
    free(cache_path);
    return ret;
}


int rpc_fsync(void *userdata, const char *path, struct fuse_file_info *fi) {
    // Force a flush of file data.
    // flushes modified file data and metadata from buffer to disk,
    // so that all changed information can be retrieved even if the system
    // crashes or is rebooted.
    DLOG("fsync is called from client...");
    int ARG_COUNT = 3;
    void **args = (void **)malloc(ARG_COUNT * sizeof(void *));
    int arg_types[ARG_COUNT + 1];
    int pathlen = strlen(path) + 1;

    arg_types[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) pathlen;
    arg_types[1] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) sizeof(struct fuse_file_info);
    arg_types[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u) ;
    arg_types[3] = 0;

    args[0] = (void *)path;
    args[1] = (void *) fi;
    int fsync_ret;
    args[2] = (void *) &fsync_ret;

    int rpc_ret = rpcCall((char *)"fsync", arg_types, args);
    int fxn_ret = 0;
    if (rpc_ret < 0) {
        DLOG( "rpcCall_fsync fail");
        fxn_ret = -EINVAL;
    } else {
        fxn_ret = fsync_ret;
    }

    if (fxn_ret < 0) {
        DLOG("Imply fsync on server fail");
    }

    free(args);
    DLOG("Return code from server : %d", fxn_ret);
    return fxn_ret;
}


// ------------------------------- CHANGE METADATA
int watdfs_cli_utimens(void *userdata, const char *path, const struct timespec ts[2]){
    /* cout << "Before call watdfs_cli_utimens, ts[0]=" << ts[0].tv_sec << "; ts[1]=" \
        << ts[1].tv_sec << std::endl; */
    struct stat *stat_server = (struct stat *)malloc(sizeof(struct stat));
    int ret, fxn_ret = 0;
    char *cache_path = get_cache_path(userdata, path);

    // check open
    if(!is_open(userdata, cache_path)){
        ret = rpc_getattr((void *)userdata, path, stat_server);
        if(ret < 0){  // server端文件不存在
            fxn_ret = ret;
            DLOG("file not exist on server, error code is: %d", fxn_ret);
        }else{
            ret = watdfs_cli_download(userdata, path);
            int fh_ret = open(cache_path, O_RDWR);
            if(fh_ret < 0){
                // 不允许用写的方式打开
                fxn_ret = fh_ret;
            }else{
                ret = utimensat(0, cache_path, ts, 0);
                if(ret < 0){
                    fxn_ret = -errno;
                    DLOG("utimensat fail during utimensat, error code is :%d", fxn_ret);
                }
                ret = close(fh_ret);
            }
        }
    }else{
        // check flag
        if(get_flag(userdata, cache_path) == O_RDONLY){
            DLOG("read modle can't do utimentsats");
            free(stat_server);
            free(cache_path);
            return -EMFILE;
        }
        ret = utimensat(0, cache_path, ts, 0);
        if(ret < 0){
            fxn_ret = -errno;
            DLOG("utimensat fail during utimensat, error code is :%d", fxn_ret);
        }else{
            if(!is_fresh(userdata, path)){
                ret = watdfs_cli_upload(userdata, path);
                if(ret < 0){   // server 没有写的权限
                    fxn_ret = ret;
                    DLOG("utimensat fail during utimensat, error code is :%d", fxn_ret);
                }else{
                    reset_tc(userdata, cache_path);
                }
            }
        }
    }

    free(stat_server);
    free(cache_path);
    return fxn_ret;
}


int rpc_utimens(void *userdata, const char *path,
                       const struct timespec ts[2]) {
    // Change file access and modification times.
    DLOG("utimens is called from client...");
    int ARG_COUNT = 3;
    void **args = (void **)malloc(ARG_COUNT * sizeof(void *));
    int arg_types[ARG_COUNT + 1];
    int pathlen = strlen(path) + 1;

    arg_types[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) pathlen;
    arg_types[1] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) 2*sizeof(struct timespec);
    arg_types[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u) ;
    arg_types[3] = 0;

    args[0] = (void *)path;
    args[1] = (void *) ts;
    int utimens_ret;
    args[2] = (void *) &utimens_ret;

    int rpc_ret = rpcCall((char *)"utimens", arg_types, args);
    int fxn_ret = 0;
    if (rpc_ret < 0) {
        DLOG( "rpcCall_utimens fail");
        fxn_ret = -EINVAL;
    } else {
        fxn_ret = utimens_ret;
    }

    if (fxn_ret < 0) {
        DLOG("Imply utimens on server fail");
    }

    free(args);
    DLOG("Return code from server : %d", fxn_ret);
    return fxn_ret;
}


