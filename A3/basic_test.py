import os
import stat
import sys
import time

# test 1
# on client
# test t-tc < T and T_client = T_server, not do cache again
f = os.open("/tmp/rzeng/mount/t3.txt", os.O_RDONLY)
os.read(f, 5)
time.sleep(1)
os.read(f, 5)
os.close(f)

# test 2
# onclient
# test t-tc > T, do cache again
f = os.open("/tmp/rzeng/mount/t3.txt", os.O_RDONLY)
os.read(f, 5)
time.sleep(6)
os.read(f, 5)
os.close(f)

# test 3
# on client
# test t-tc < T and T_client != T_server, do cache again
f = os.open("/tmp/rzeng/mount/t3.txt", os.O_RDONLY)
os.read(f, 5)
print("change file on server during sleep")
time.sleep(2)
os.read(f, 5)
os.close(f)

# on server
f = os.open("/tmp/rzeng/server/t3.txt", os.O_RDWR)
os.write(f, string("change the content on server"))
os.close(f)

# test 4
# on client
# on client open file multiple times, return -EMFILE
f = os.open("/tmp/rzeng/mount/t3.txt", os.O_RDONLY)
os.read(f, 5)
fd = f = os.open("/tmp/rzeng/mount/t3.txt",os.O_RDONLY)
os.close(f)
os.close(fd)

# test 5
# on client A
# one client open file with write, another client also open file with write mode
f = os.open("/tmp/rzeng/mount/t3.txt", os.O_RDWR)
os.read(f, 5)
print("open same file on client B with write mode")
os.sleep(3)
os.close(f)

# on client B, open another client port
# expect return -EACCES, because two clients try to write one file
fd = f = os.open("/tmp/rzeng/mount/t3.txt", os.O_RDWR)
os.close(fd)

# test 6
# on client A
# one client open file with write, another client open file with read mode
f = os.open("/tmp/rzeng/mount/t3.txt",os.O_RDONLY)
os.read(f, 5)
print("open same file on client B with write mode")
os.sleep(3)
os.close(f)

# on client B, open another client port
# expect return 0, because only one client try to write, no write conflict
fd = f = os.open("/tmp/rzeng/mount/t3.txt", os.O_RDWR)
os.write(fd, string("open file with write mode"))
os.close(fd)

