//
// Created by 江月如梦 on 2020-03-08.
//
#include "rw_lock.h"

#ifndef A3_RPCCALL_H
#define A3_RPCCALL_H

#endif //A3_RPCCALL_H

//#include "watdfs_client.h"

// helper
// struct timespec get_curr_time()；
int get_flag(void *userdata, char *cache_path);
int get_fh(void *userdata, char *cache_path);
time_t get_tc(void *userdata, char *cache_path);
char *get_cache_path(void *userdata, const char *short_path);
void reset_tc(void *userdata, char *cache_path);
void update_client_metadata(void *userdata, char *cache_path, struct timespec mtime);
void update_server_metadata(void *userdata, char *cache_path, char *path, struct timespec mtime);
int open_local_file(void *userdata, char *cache_path, int flags);
int close_client(void *userdata, char *cache_path);
bool is_open(void *userdata, char *cache_path);
bool is_fresh(void *userdata, const char *path);


// define all rpc operations
int rpc_lock(const char *path, rw_lock_mode_t lock_mode);

int rpc_unlock(const char *path, rw_lock_mode_t unlock_mode);

int rpc_fgetattr(void *userdata, const char *path, struct stat *statbuf, struct fuse_file_info *fi);

int rpc_getattr(void *userdata, const char *path, struct stat *statbuf);

int rpc_mknod(void *userdata, const char *path, mode_t mode, dev_t dev);

int rpc_open(void *userdata, const char *path, struct fuse_file_info *fi);

int rpc_release(void *userdata, const char *path, struct fuse_file_info *fi);

int rpc_read(void *userdata, const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi);

int rpc_write(void *userdata, const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi);

int rpc_truncate(void *userdata, const char *path, off_t newsize);

int rpc_fsync(void *userdata, const char *path, struct fuse_file_info *fi);

int rpc_utimens(void *userdata, const char *path, const struct timespec ts[2]);