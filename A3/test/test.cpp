#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <cstdio>
#include <cstring>
#include <string>

using namespace std;

const char *fn = "/tmp/j892zhan/mount/modify.txt";

int main() {
  int fd = open(fn, O_WRONLY);
  if (fd <= 0) {
    printf("can't open %d %d %s\n", fd, errno, strerror(errno));
  }

  printf("sleep\n");
  sleep(10);
  printf("waked up\n");

  // struct stat local_stat;
  // int ret = stat(fn, &local_stat);
  // // local_stat.st_atim.tv_nsec = UTIME_OMIT;
  // local_stat.st_mtim.tv_sec -= 7200;
  // struct timespec ts[2] = {local_stat.st_atim, local_stat.st_mtim};

  // printf("args path: %s, [0].tv_sec: %ld, [0].tv_nsec: %ld, [1].tv_sec: %ld, "
  // "[1].tv_nsec: %ld\n",
  // fn, ts[0].tv_sec, ts[0].tv_nsec, ts[1].tv_sec, ts[1].tv_nsec);

  // ret = utimensat(0, fn, ts, 0);
  // if (ret <= 0) {
  //   printf("syscall fail %d %d %s\n", ret, errno, strerror(errno));
  // }
  close(fd);
  return 0;
}
