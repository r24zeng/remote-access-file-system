import os
from os import path

import pytest


MOUNT_DIR = '/tmp/j892zhan/mount'


def with_suffix(suffix):
    if suffix:
        return MOUNT_DIR + suffix
    return MOUNT_DIR


def filename_exists(suffix=None):
    return path.join(with_suffix(suffix), 'exists.txt')


def filename_notexists(suffix=None):
    return path.join(with_suffix(suffix), 'notexists.txt')


def filename_modify(suffix=None):
    return path.join(with_suffix(suffix), 'modify.txt')


def filename_restore(fn):
    return fn.replace('mount1', 'server').replace('mount2', 'server').replace('mount', 'server')



fn = filename_notexists()
fd = os.open(fn, os.O_CREAT)
assert fd > 0
os.close(fd)




# fn = path.join(MOUNT_DIR, 'modify.txt')
# fd = os.open(fn, os.O_CREAT)
#     content = \
# b'''0123456789
# abcdefghijklmnopqrst
# uvwxyz'''
#     os.pwrite(fd, content, 0)
# print(os.pread(fd, 100, 0))
# os.truncate(fn, 11)
# os.utime(fn, (time.time() - 36000, time.time() - 7200))
# os.utime(fn, ns=(time.time() * 10**9, 1073741822))
# os.fsync(fd)
# os.close(fd)
