import os
from os import path
import time

import pytest


MOUNT_DIR = '/tmp/j892zhan/mount'


def with_suffix(suffix):
    if suffix:
        return MOUNT_DIR + suffix
    return MOUNT_DIR


def filename_exists(suffix=None):
    return path.join(with_suffix(suffix), 'exists.txt')


def filename_notexists(suffix=None):
    return path.join(with_suffix(suffix), 'notexists.txt')


def filename_modify(suffix=None):
    return path.join(with_suffix(suffix), 'modify.txt')


def filename_restore(fn):
    return fn.replace('mount1', 'server').replace('mount2', 'server').replace('mount', 'server')


@pytest.mark.run(order=1)
def test_getattr_exists():
    fn = filename_exists()
    st = os.stat(fn)
    # 2019-03-10 12:16:40.081129341 -0400
    assert round(st.st_mtime) == 1552234600


@pytest.mark.run(order=2)
def test_getattr_notexists():
    fn = filename_notexists()
    with pytest.raises(OSError) as e:
        os.stat(fn)
    assert e.value.errno == 2


@pytest.mark.run(order=10)
def test_open_exists():
    fn = filename_exists()
    fd = os.open(fn, os.O_RDONLY)
    assert fd > 0
    os.close(fd)

    fd = os.open(fn, os.O_WRONLY)
    assert fd > 0
    os.close(fd)

    fd = os.open(fn, os.O_RDWR)
    assert fd > 0
    os.close(fd)


@pytest.mark.run(order=11)
def test_open_with_create_excl():
    fn = filename_notexists()
    fd = os.open(fn, os.O_CREAT)
    assert fd > 0
    os.close(fd)
    fn_server = filename_restore(fn)
    assert path.isfile(fn_server)

    with pytest.raises(OSError) as e:
        os.open(fn, os.O_CREAT | os.O_EXCL)
    assert e.value.errno == 17
    os.remove(fn_server)


@pytest.mark.run(order=20)
def test_fgetattr_goodfd():
    fn = filename_exists()
    fd = os.open(fn, os.O_RDONLY)
    assert fd > 0

    st = os.fstat(fd)
    # 2019-03-10 12:16:40.081129341 -0400
    assert round(st.st_mtime) == 1552234600
    os.close(fd)


@pytest.mark.run(order=21)
def test_fgetattr_badfd():
    fn = filename_exists()
    fd = os.open(fn, os.O_RDONLY)
    assert fd > 0
    bad_fd = fd + 10
    with pytest.raises(OSError) as e:
        os.fstat(bad_fd)
    assert e.value.errno == 9
    os.close(fd)


@pytest.mark.run(order=30)
def test_open_mult():
    fd1 = os.open(filename_exists('1'), os.O_RDONLY)
    assert fd1 > 0
    with pytest.raises(OSError) as e:
        os.open(filename_exists('1'), os.O_RDONLY)
    assert e.value.errno == 24
    os.close(fd1)
    time.sleep(1)


@pytest.mark.run(order=31)
def test_opened_in_r_read():
    fd1 = os.open(filename_exists('1'), os.O_RDONLY)
    assert fd1 > 0

    fd2 = os.open(filename_exists('2'), os.O_RDONLY)
    assert fd2 > 0
    os.close(fd2)
    os.close(fd1)
    time.sleep(1)


@pytest.mark.run(order=32)
def test_opened_in_r_write():
    fd1 = os.open(filename_exists('1'), os.O_RDONLY)
    assert fd1 > 0

    fd2 = os.open(filename_exists('2'), os.O_WRONLY)
    assert fd2 > 0
    os.close(fd2)

    fd2 = os.open(filename_exists('2'), os.O_RDWR)
    assert fd2 > 0
    os.close(fd2)
    os.close(fd1)
    time.sleep(1)


@pytest.mark.run(order=35)
def test_opened_in_w():
    def run():
        with pytest.raises(OSError) as e:
            fd2 = os.open(filename_exists('2'), os.O_WRONLY)
        assert e.value.errno == 13
        with pytest.raises(OSError) as e:
            fd2 = os.open(filename_exists('2'), os.O_RDWR)
        assert e.value.errno == 13
        with pytest.raises(OSError) as e:
            fd2 = os.open(filename_exists('2'), os.O_APPEND | os.O_WRONLY)
        assert e.value.errno == 13
        fd2 = os.open(filename_exists('2'), os.O_RDONLY)
        assert fd2 > 0
        os.close(fd2)

    fd1 = os.open(filename_exists('1'), os.O_WRONLY)
    assert fd1 > 0
    run()
    os.close(fd1)

    fd1 = os.open(filename_exists('1'), os.O_RDWR)
    assert fd1 > 0
    run()
    os.close(fd1)

    fd1 = os.open(filename_exists('1'), os.O_APPEND | os.O_WRONLY)
    assert fd1 > 0
    run()
    os.close(fd1)
    time.sleep(1)


@pytest.mark.run(order=40)
def test_write_read_wait_read():
    fn = filename_modify()

    fd = os.open(fn, os.O_CREAT | os.O_EXCL | os.O_WRONLY)
    os.write(fd, b'123')
    os.close(fd)
    fd = os.open(fn, os.O_RDONLY)
    assert os.pread(fd, 100, 0) == b'123'
    time.sleep(1)
    with open(filename_restore(fn), 'a') as f:
        print('456', end='', file=f)
    time.sleep(1)
    assert os.pread(fd, 100, 0) == b'123'
    time.sleep(3)
    assert os.pread(fd, 100, 0) == b'123456'
    os.close(fd)
    os.remove(filename_restore(fn))


@pytest.mark.run(order=41)
def test_write_after_stale():
    fn = filename_modify()
    fnori = filename_restore(fn)

    def check(content):
        with open(fnori) as f:
            assert f.read() == content

    fd = os.open(fn, os.O_CREAT | os.O_EXCL | os.O_WRONLY)
    check('')
    os.write(fd, b'123')
    check('')
    time.sleep(5)
    os.write(fd, b'456')
    check('123456')
    os.close(fd)
    time.sleep(1)
    os.remove(fnori)


@pytest.mark.run(order=42)
def test_synced_to_server_when_close():
    fn = filename_modify()
    fnori = filename_restore(fn)

    def check(content):
        with open(fnori) as f:
            assert f.read() == content

    fd = os.open(fn, os.O_CREAT | os.O_EXCL | os.O_WRONLY)
    check('')
    os.write(fd, b'123')
    check('')
    os.close(fd)
    time.sleep(1)
    check('123')
    os.remove(fnori)
    time.sleep(1)


@pytest.mark.run(order=43)
def test_fsync_when_fresh():
    fn = filename_modify()
    fnori = filename_restore(fn)

    def check(content):
        with open(fnori) as f:
            assert f.read() == content

    fd = os.open(fn, os.O_CREAT | os.O_EXCL | os.O_WRONLY)
    check('')
    os.write(fd, b'123')
    check('')
    os.fsync(fd)
    check('123')
    os.close(fd)
    time.sleep(1)
    os.remove(fnori)


@pytest.mark.run(order=44)
def test_2_clients_write_read():
    fnr = filename_modify('1')
    fnw = filename_modify('2')
    with open(filename_restore(fnr), 'w') as f:
        print('000', end='', file=f)

    fdw = os.open(fnw, os.O_APPEND | os.O_WRONLY)
    fdr = os.open(fnr, os.O_RDONLY)
    assert os.pread(fdr, 100, 0) == b'000'
    os.write(fdw, b'123')
    os.fsync(fdw)
    os.close(fdw)
    with open(filename_restore(fnr)) as f:
        assert f.read() == '000123'
    t = time.time()
    while 1:
        assert os.pread(fdr, 100, 0) == b'000'
        time.sleep(1)
        if time.time() - t >= 5:
            break
    assert os.pread(fdr, 100, 0) == b'000123'
    os.close(fdr)
    time.sleep(1)
    os.remove(filename_restore(fnr))
    time.sleep(1)
