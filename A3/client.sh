#!/bin/bash

export CACHE_INTERVAL_SEC=5  
mkdir -p /tmp/rzeng/cache /tmp/rzeng/mount
fusermount -u /tmp/rzeng/mount
./watdfs_client -s -f -o nonempty /tmp/rzeng/cache /tmp/rzeng/mount
./watdfs_client -s -f -o direct_io /tmp/rzeng/cache /tmp/rzeng/mount
