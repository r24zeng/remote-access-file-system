#include "rpc.h"

// You may need to change your includes depending on whether you use C or C++.

// Needed for stat.
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fuse.h>

// Needed for errors.
#include <errno.h>

// Needed for string operations.
#include <cstring>

// Need malloc and free.
#include <cstdlib>

// You may want to include iostream or cstdio.h if you print to standard error.
#include "printer.h"

// Global state server_persist_dir.
char *server_persist_dir = NULL;

// We need to operate on the path relative to the the server_persist_dir.
// This function returns a path that appends the given short path to the
// server_persist_dir. The character array is allocated on the heap, therefore
// it should be freed after use.
char* get_full_path(char *short_path) {
  int short_path_len = strlen(short_path);
  int dir_len = strlen(server_persist_dir);
  int full_len = dir_len + short_path_len + 1;

  char *full_path = (char*)malloc(full_len);

  // First fill in the directory.
  strcpy(full_path, server_persist_dir);
  // Then append the path.
  strcat(full_path, short_path);

  return full_path;
}

// The server implementation of getattr.
int watdfs_getattr(int *argTypes, void **args) {
  // Get the arguments.
  // The first argument is the path relative to the mountpoint.
  char *short_path = (char*)args[0];
  // The second argument is the stat structure, which should be filled in
  // by this function.
  struct stat *statbuf = (struct stat*)args[1];
  // The third argument is the return code, which will be 0, or -errno.
  int *ret = (int*)args[2];

  // Get the local file name, so we call our helper function which appends
  // the server_persist_dir to the given path.
  char *full_path = get_full_path(short_path);

  // Initially we set set the return code to be 0.
  *ret = 0;

  // Make the stat system call, which is the corresponding system call needed
  // to support getattr. You should make the stat system call here:
  int stat_ret = stat( full_path, statbuf );

  // Let sys_ret be the return code from the stat system call.
  int sys_ret = 0;
  if (stat_ret < 0) {
    sys_ret = -EINVAL;
  } else {
    sys_ret = stat_ret;
    print("SUCCESS: getattr system call with code: ", sys_ret);
  }

  // You should use the statbuf as an argument to the stat system call, but it
  // is currently unused.
  (void)statbuf;

  if (sys_ret < 0) {
    // If there is an error on the system call, then the return code should
    // be -errno.
    *ret = -errno;
  }

  // Clean up the full path, it was allocated on the heap.
  free(full_path);

  // The RPC call should always succeed, so return 0.
  return 0;
}
int watdfs_fgetattr(int *argTypes, void **args) {
  // Get the arguments.
  char *short_path = (char*)args[0];
  struct stat *statbuf = (struct stat*)args[1];
  struct fuse_file_info *fi = (struct fuse_file_info*)args[2];
  int *ret = (int*)args[3];

  // Get the local file name, so we call our helper function which appends
  // the server_persist_dir to the given path.
  char *full_path = get_full_path(short_path);

  // Initially we set set the return code to be 0.
  *ret = 0;

  int stat_ret = fstat(fi->fh, statbuf);
  
  int sys_ret = 0;
  if (stat_ret < 0) {
    sys_ret = -EINVAL;
  } else {
    sys_ret = stat_ret;
    print("SUCCESS: fgetattr system call with code: ", sys_ret);
  }
		
	if (sys_ret < 0) {
    *ret = -errno;
  }

  (void)statbuf;
  (void)fi;

  // Clean up the full path, it was allocated on the heap.
  free(full_path);

	return 0;
}

int watdfs_mknod(int *argTypes, void **args) {
  // Get the arguments.
  char *short_path = (char*)args[0];
  mode_t *mode = (mode_t*)args[1];
  dev_t *dev = (dev_t*)args[2];
  int *ret = (int*)args[3];

  // Get the local file name, so we call our helper function which appends
  // the server_persist_dir to the given path.
  char *full_path = get_full_path(short_path);

  // Initially we set set the return code to be 0.
  *ret = 0;

  int fuse_ret = mknod(full_path, *mode, *dev);
  
  if (fuse_ret < 0) {
    *ret = -errno;
  } else {
    *ret = fuse_ret;
    print("SUCCESS: mknod system call with code: ", fuse_ret);
  }

  (void)mode;
  (void)dev;

  // Clean up the full path, it was allocated on the heap.
  free(full_path);

	return 0;
}
int watdfs_open(int *argTypes, void **args) {
  // Get the arguments.
  char *short_path = (char*)args[0];
  struct fuse_file_info *fi = (struct fuse_file_info*)args[1];
  int *ret = (int*)args[2];

  // Get the local file name, so we call our helper function which appends
  // the server_persist_dir to the given path.
  char *full_path = get_full_path(short_path);

  // Initially we set set the return code to be 0.
  *ret = 0;

  int fd = open(full_path, fi->flags);
  
  if (fd < 0) {
    *ret = -errno;
  } else {
    fi->fh = fd;
    print("SUCCESS: open system call with code: ", fd);
  }

  // Clean up the full path, it was allocated on the heap.
  free(full_path);

	return 0;
}
int watdfs_release(int *argTypes, void **args) {
  // Get the arguments.
  char *short_path = (char*)args[0];
  struct fuse_file_info *fi = (struct fuse_file_info*)args[1];
  int *ret = (int*)args[2];

  // Get the local file name, so we call our helper function which appends
  // the server_persist_dir to the given path.
  char *full_path = get_full_path(short_path);

  // Initially we set set the return code to be 0.
  *ret = 0;

  int fuse_ret = close(fi->fh);
  
  if (fuse_ret < 0) {
    *ret = -errno;
  } else {
    *ret = fuse_ret;
    print("SUCCESS: release system call with code: ", fuse_ret);
  }

  (void)fi;

  // Clean up the full path, it was allocated on the heap.
  free(full_path);

	return 0; 
}

int watdfs_write(int *argTypes, void **args) {
  // Get the arguments.
  char *short_path = (char*)args[0];
  char *buf = (char*)args[1];
  size_t *size = (size_t*)args[2];
  off_t *offset = (off_t*)args[3]; 
  struct fuse_file_info *fi = (struct fuse_file_info*)args[4];
  int *ret = (int*)args[5];

  // Get the local file name, so we call our helper function which appends
  // the server_persist_dir to the given path.
  char *full_path = get_full_path(short_path);

  // Initially we set set the return code to be 0.
  *ret = 0;
  
  int res = pwrite(fi->fh, buf, *size, *offset);

  if (res < 0) {
    *ret = -errno;
  } else {
    *ret = res;
    print("SUCCESS: write system call with code: ", res);
  }

  // Clean up the full path, it was allocated on the heap.
  free(full_path);

	return 0;
}
int watdfs_read(int *argTypes, void **args) {
  // Get the arguments.
  char *short_path = (char*)args[0];
  char *buf = (char*)args[1];
  size_t *size = (size_t*)args[2];
  off_t *offset = (off_t*)args[3]; 
  struct fuse_file_info *fi = (struct fuse_file_info*)args[4];
  int *ret = (int*)args[5];

  // Get the local file name, so we call our helper function which appends
  // the server_persist_dir to the given path.
  char *full_path = get_full_path(short_path);

  // Initially we set set the return code to be 0.
  *ret = 0;

  int res = pread(fi->fh, buf, *size, *offset);

  if (res < 0) {
    *ret = -errno;
  } else {
    *ret = res;
    print("SUCCESS: read system call with code: ", res);
  }

  // Clean up the full path, it was allocated on the heap.
  free(full_path);

	return 0;
}
int watdfs_truncate(int *argTypes, void **args) {
  // Get the arguments.
  char *short_path = (char*)args[0];
  off_t *newsize = (off_t*)args[1];
  int *ret = (int*)args[2];

  // Get the local file name, so we call our helper function which appends
  // the server_persist_dir to the given path.
  char *full_path = get_full_path(short_path);

  // Initially we set set the return code to be 0.
  *ret = 0;

  int fuse_ret = truncate(full_path, *newsize);
  
  if (fuse_ret < 0) {
    *ret = -errno;
  } else {
    *ret = fuse_ret;
    print("SUCCESS: truncate system call with code: ", fuse_ret);
  }

  (void)newsize;

  // Clean up the full path, it was allocated on the heap.
  free(full_path);

	return 0;
}

int watdfs_fsync(int *argTypes, void **args) {
  // Get the arguments.
  char *short_path = (char*)args[0];
  struct fuse_file_info *fi = (struct fuse_file_info*)args[1];
  int *ret = (int*)args[2];

  // Get the local file name, so we call our helper function which appends
  // the server_persist_dir to the given path.
  char *full_path = get_full_path(short_path);

  // Initially we set set the return code to be 0.
  *ret = 0;

  int fuse_ret = fsync(fi->fh);
  
  if (fuse_ret < 0) {
    *ret = -errno;
  } else {
    *ret = fuse_ret;
    print("SUCCESS: fsync system call with code: ", fuse_ret);
  }

  (void)fi;

  // Clean up the full path, it was allocated on the heap.
  free(full_path);

	return 0;
}

int watdfs_utimens(int *argTypes, void **args){
  // Get the arguments.
  char *short_path = (char*)args[0];
  struct timespec *ts = (struct timespec *)args[1];
  int *ret = (int*)args[2];

  // Get the local file name, so we call our helper function which appends
  // the server_persist_dir to the given path.
  char *full_path = get_full_path(short_path);

  // Initially we set set the return code to be 0.
  *ret = 0;

  int fuse_ret = utimensat(0, full_path, ts, 0);
  
  if (fuse_ret < 0) {
    *ret = -errno;
  } else {
    *ret = fuse_ret;
    print("SUCCESS: utimens system call with code: ", fuse_ret);
  }

  (void)ts[2];

  // Clean up the full path, it was allocated on the heap.
  free(full_path);

	return 0;
}


// The main function of the server.
int main(int argc, char *argv[]) {
  // argv[1] should contain the directory where you should store data on the
  // server. If it is not present it is an error, that we cannot recover from.
  if (argc != 2) {
    // In general you shouldn't print to stderr or stdout, but it may be
    // helpful here for debugging. Important: Make sure you turn off logging
    // prior to submission!
    // See watdfs_client.c for more details
    // # ifdef PRINT_ERR
    // fprintf(stderr, "Usage: %s server_persist_dir\n", argv[0]);
    // Or if you prefer c++:
    // std::cerr << "Usaage:" << argv[0] << " server_persist_dir";
    // #endif
    #if defined ( PRINT_ERR )
    std::cerr << "Usaage:" << argv[0] << " server_persist_dir" << std::endl;
    #endif
    return -1;
  }
  // Store the directory in a global variable.
  server_persist_dir = argv[1];

  // Initialize the rpc library by calling rpcServerInit. You should call
  // rpcServerInit here:
  int ret = rpcServerInit();

  // If there is an error with rpcServerInit, it maybe useful to have
  // debug-printing here, and then you should return.
  if (ret < 0) {
    print("FAIL: initialize RPC Client with code: ", ret);
    return ret;
  }

  // Register your functions with the RPC library.
  { // GETATTR
    int argTypes[4];
    // path: input, array, ARG_CHAR
    argTypes[0] = (1 << ARG_INPUT) | (1 << ARG_ARRAY) | (ARG_CHAR << 16) | 1;
    // Note for arrays we can set the length to be anything  > 1.

    // statbuf: output, array, ARG_CHAR
    argTypes[1] = (1 << ARG_OUTPUT) | (1 << ARG_ARRAY) | (ARG_CHAR << 16) | 1;
    // retcode: output, ARG_INT
    argTypes[2] = (1 << ARG_OUTPUT) | (ARG_INT << 16);
    // null terminator
    argTypes[3] = 0;

    // We need to register the function with the types and the name.
    ret = rpcRegister((char*)"getattr", argTypes, watdfs_getattr);
    if (ret < 0) {
      // It may be useful to have debug-printing here.
      print("FAIL: getattr unregistered with code: ", ret);
      return ret;
    }
  }
  { // FGETATTR
    int argTypes[5];
    // path: input, array, ARG_CHAR
    argTypes[0] = (1 << ARG_INPUT) | (1 << ARG_ARRAY) | (ARG_CHAR << 16) | 1;
    // statbuf: output, array, ARG_CHAR
    argTypes[1] = (1 << ARG_OUTPUT) | (1 << ARG_ARRAY) | (ARG_CHAR << 16) | 1;
    // fi: input, array, ARG_CHAR
    argTypes[2] = (1 << ARG_INPUT) | (1 << ARG_ARRAY) | (ARG_CHAR << 16) | 1;
    // retcode: output, ARG_INT
    argTypes[3] = (1 << ARG_OUTPUT) | (ARG_INT << 16);
    // null terminator
    argTypes[4] = 0;

    // We need to register the function with the types and the name.
    ret = rpcRegister((char*)"fgetattr", argTypes, watdfs_fgetattr);
    if (ret < 0) {
      // It may be useful to have debug-printing here.
      print("FAIL: fgetattr unregistered with code: ", ret);
      return ret;
    }
  }
  { // MKNOD
    int argTypes[5];
    // path: input, array, ARG_CHAR
    argTypes[0] = (1 << ARG_INPUT) | (1 << ARG_ARRAY) | (ARG_CHAR << 16) | 1;
    // mode: input, ARG_INT
    argTypes[1] = (1 << ARG_INPUT) | (ARG_INT << 16);
    // dev: input, ARG_LONG
    argTypes[2] = (1 << ARG_INPUT) | (ARG_LONG << 16);
    // retcode: output, ARG_INT
    argTypes[3] = (1 << ARG_OUTPUT) | (ARG_INT << 16);
    // null terminator
    argTypes[4] = 0;

    // We need to register the function with the types and the name.
    ret = rpcRegister((char*)"mknod", argTypes, watdfs_mknod);
    if (ret < 0) {
      // It may be useful to have debug-printing here.
      print("FAIL: mknod unregistered with code: ", ret);
      return ret;
    }
  }
  { // OPEN
    int argTypes[4];
    // path: input, array, ARG_CHAR
    argTypes[0] = (1 << ARG_INPUT) | (1 << ARG_ARRAY) | (ARG_CHAR << 16) | 1;
    // fi: input, output, array, ARG_CHAR
    argTypes[1] = (1 << ARG_INPUT) | (1 << ARG_OUTPUT) | (1 << ARG_ARRAY) | (ARG_CHAR << 16) | 1;
    // retcode: output, ARG_INT
    argTypes[2] = (1 << ARG_OUTPUT) | (ARG_INT << 16);
    // null terminator
    argTypes[3] = 0;

    // We need to register the function with the types and the name.
    ret = rpcRegister((char*)"open", argTypes, watdfs_open);
    if (ret < 0) {
      // It may be useful to have debug-printing here.
      print("FAIL: open unregistered with code: ", ret);
      return ret;
    }
  }
  { // CLOSE
    int argTypes[4];
    // path: input, array, ARG_CHAR
    argTypes[0] = (1 << ARG_INPUT) | (1 << ARG_ARRAY) | (ARG_CHAR << 16) | 1;
    // fi: input, array, ARG_CHAR
    argTypes[1] = (1 << ARG_INPUT) | (1 << ARG_ARRAY) | (ARG_CHAR << 16) | 1;
    // retcode: output, ARG_INT
    argTypes[2] = (1 << ARG_OUTPUT) | (ARG_INT << 16);
    // null terminator
    argTypes[3] = 0;

    // We need to register the function with the types and the name.
    ret = rpcRegister((char*)"release", argTypes, watdfs_release);
    if (ret < 0) {
      // It may be useful to have debug-printing here.
      print("FAIL: release unregistered with code: ", ret);
      return ret;
    }
  }
  { // WRITE
    int argTypes[7];
    // path: input, array, ARG_CHAR
    argTypes[0] = (1 << ARG_INPUT) | (1 << ARG_ARRAY) | (ARG_CHAR << 16) | 1;
    // buf: input, array, ARG_CHAR
    argTypes[1] = (1 << ARG_INPUT) | (1 << ARG_ARRAY) | (ARG_CHAR << 16) | 1;
    // size: input, ARG_LONG
    argTypes[2] = (1 << ARG_INPUT) | (ARG_LONG << 16);
    // offset: input, ARG_LONG
    argTypes[3] = (1 << ARG_INPUT) | (ARG_LONG << 16);
    // fi: input, array, ARG_CHAR
    argTypes[4] = (1 << ARG_INPUT) | (1 << ARG_ARRAY) | (ARG_CHAR << 16) | 1;
    // retcode: output, ARG_INT
    argTypes[5] = (1 << ARG_OUTPUT) | (ARG_INT << 16);
    // null terminator
    argTypes[6] = 0;

    // We need to register the function with the types and the name.
    ret = rpcRegister((char*)"write", argTypes, watdfs_write);
    if (ret < 0) {
      // It may be useful to have debug-printing here.
      print("FAIL: write unregistered with code: ", ret);
      return ret;
    }
  }
  { // READ
    int argTypes[7];
    // path: input, array, ARG_CHAR
    argTypes[0] = (1 << ARG_INPUT) | (1 << ARG_ARRAY) | (ARG_CHAR << 16) | 1;
    // buf: output, array, ARG_CHAR
    argTypes[1] = (1 << ARG_OUTPUT) | (1 << ARG_ARRAY) | (ARG_CHAR << 16) | 1;
    // size: input, ARG_LONG
    argTypes[2] = (1 << ARG_INPUT) | (ARG_LONG << 16);
    // offset: input, ARG_LONG
    argTypes[3] = (1 << ARG_INPUT) | (ARG_LONG << 16);
    // fi: input, array, ARG_CHAR
    argTypes[4] = (1 << ARG_INPUT) | (1 << ARG_ARRAY) | (ARG_CHAR << 16) | 1;
    // retcode: output, ARG_INT
    argTypes[5] = (1 << ARG_OUTPUT) | (ARG_INT << 16);
    // null terminator
    argTypes[6] = 0;

    // We need to register the function with the types and the name.
    ret = rpcRegister((char*)"read", argTypes, watdfs_read);
    if (ret < 0) {
      // It may be useful to have debug-printing here.
      print("FAIL: read unregistered with code: ", ret);
      return ret;
    }
  }
  { // TRUNCATE
    int argTypes[4];
    // path: input, array, ARG_CHAR
    argTypes[0] = (1 << ARG_INPUT) | (1 << ARG_ARRAY) | (ARG_CHAR << 16) | 1;
    // newsize: input, ARG_LONG
    argTypes[1] = (1 << ARG_INPUT) | (ARG_LONG << 16);
    // retcode: output, ARG_INT
    argTypes[2] = (1 << ARG_OUTPUT) | (ARG_INT << 16);
    // null terminator
    argTypes[3] = 0;

    // We need to register the function with the types and the name.
    ret = rpcRegister((char*)"truncate", argTypes, watdfs_truncate);
    if (ret < 0) {
      // It may be useful to have debug-printing here.
      print("FAIL: truncate unregistered with code: ", ret);
      return ret;
    }
  }
  { // UTIMENS
    int argTypes[4];
    // path: input, array, ARG_CHAR
    argTypes[0] = (1 << ARG_INPUT) | (1 << ARG_ARRAY) | (ARG_CHAR << 16) | 1;
    // ts: input, array, ARG_CHAR
    argTypes[1] = (1 << ARG_INPUT) | (1 << ARG_ARRAY) | (ARG_CHAR << 16) | 1;
    // retcode: output, ARG_INT
    argTypes[2] = (1 << ARG_OUTPUT) | (ARG_INT << 16);
    // null terminator
    argTypes[3] = 0;

    // We need to register the function with the types and the name.
    ret = rpcRegister((char*)"utimens", argTypes, watdfs_utimens);
    if (ret < 0) {
      // It may be useful to have debug-printing here.
      print("FAIL: utimens unregistered with code: ", ret);
      return ret;
    }
  }
  { // FSYNC
    int argTypes[4];
    // path: input, array, ARG_CHAR
    argTypes[0] = (1 << ARG_INPUT) | (1 << ARG_ARRAY) | (ARG_CHAR << 16) | 1;
    // fi: input, array, ARG_CHAR
    argTypes[1] = (1 << ARG_INPUT) | (1 << ARG_ARRAY) | (ARG_CHAR << 16) | 1;
    // retcode: output, ARG_INT
    argTypes[2] = (1 << ARG_OUTPUT) | (ARG_INT << 16);
    // null terminator
    argTypes[3] = 0;

    // We need to register the function with the types and the name.
    ret = rpcRegister((char*)"fsync", argTypes, watdfs_fsync);
    if (ret < 0) {
      // It may be useful to have debug-printing here.
      print("FAIL: truncate unregistered with code: ", ret);
      return ret;
    }
  }

  // Hand over control to the RPC library by calling rpcExecute. You should call
  // rpcExecute here:
  ret = rpcExecute();

  // rpcExecute could fail so you may want to have debug-printing here, and then
  // you should return.
  if (ret < 0) {
    print("FAIL: execute RPC with code: ", ret);
  }
  return ret;
}
