#include "printer.h"

using namespace std;

void print(string msg, int code) {
  #if defined ( PRINT_ERR )
  cerr << msg << code << endl;
  #endif
}